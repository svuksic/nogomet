package hr.simple.jersey.filter;

import java.util.List;

import hr.simple.entity.User;
import hr.simple.hibernate.HibernateUtil;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

public class RequestFilter implements ContainerRequestFilter {

	@Override
	public ContainerRequest filter(ContainerRequest request){
		ContainerRequest result = null;
 		if(StringUtils.equals(request.getPath(), "pageResolver/login.html") 
 				|| StringUtils.equals(request.getPath(), "loginHandler/signIn") 
 				|| StringUtils.equals(request.getPath(), "loginHandler/login")
 				|| StringUtils.equals(request.getPath(), "loginHandler/googleSignIn")
 				|| StringUtils.equals(request.getPath(), "loginHandler/sendMail")
 				|| StringUtils.equals(request.getPath(), "pageResolver/kontakt.html")){
			result = request;
 		}else{
 			if(request.getHeaderValue("Authority") != null){
 				String securityToken = request.getHeaderValue("Authority");
 				Session session = HibernateUtil.getSessionFactory().openSession();
 				Transaction tr = session.beginTransaction();
 				Query query = session.createQuery("select u from User u " +
 						" where u.token = :token ");
 				query.setParameter("token", securityToken);
 				@SuppressWarnings("unchecked")
 				List<User> userList = query.list();
 				if(userList.size() > 0){
 					result = request;
 				}else{
 					//ako nema takvog tokena trenutno u bazi vracam gresku koju u javascriptu obradjujem i pokazujem prikladnu poruku
 					result = redirectToAutentificationError(request);
 				}
 				tr.commit();
 				session.close(); 			
 			}else{			
 				result = redirectToError(request);
 			}		 			
 		}
 		return result;
	}
	
	public ContainerRequest redirectToError(ContainerRequest request){
		ContainerRequest result = null;
		String uri = request.getRequestUri().toString();
		String newUri = uri.substring(0, uri.lastIndexOf("rest/")) + "rest/pageResolver/error.html";
		request.setUris(request.getBaseUri(), UriBuilder.fromUri(newUri).build());
		result = request;
		return result;
	}
	
	public ContainerRequest redirectToAutentificationError(ContainerRequest request){
		ContainerRequest result = null;
		String uri = request.getRequestUri().toString();
		String newUri = uri.substring(0, uri.lastIndexOf("/")) + "/login.html";
		request.setUris(request.getBaseUri(), UriBuilder.fromUri(newUri).build());
		result = request;
		return result;
	}

}
