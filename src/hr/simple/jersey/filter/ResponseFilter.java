package hr.simple.jersey.filter;


import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

public class ResponseFilter implements ContainerResponseFilter {

	@Override
	public ContainerResponse filter(ContainerRequest arg0, ContainerResponse response) {
		response.getHttpHeaders().remove("WWW-Authenticate:");
		response.getHttpHeaders().add("sadas", "sad");
		return response;
	}



}
