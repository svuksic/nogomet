package hr.simple.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {
	  static SessionFactory sessionFactory;  
	  static ServiceRegistry serviceRegistry;
	        
	   public static SessionFactory createSessionFactory(){  
	    	try {  
	        	Configuration configuration = new Configuration();
	            configuration.configure();
	        	serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
	            sessionFactory = configuration.buildSessionFactory(serviceRegistry);  
	        } catch (Throwable e) {  
	            throw new ExceptionInInitializerError(e);  
	        }	  
	        return sessionFactory;  
	    } 
	   
	   public static SessionFactory getSessionFactory(){  
		   return sessionFactory;
	   }
	      
	    public static void shutDown(){  
	        //closes caches and connections  
	        getSessionFactory().close();  
	    }  
}
