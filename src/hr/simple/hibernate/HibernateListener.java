package hr.simple.hibernate;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.jdbc.connections.internal.C3P0ConnectionProvider;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;

public class HibernateListener implements ServletContextListener {
	public void contextInitialized(ServletContextEvent event) {
        HibernateUtil.createSessionFactory(); // Just call the static initializer of that class    
    }
 
    public void contextDestroyed(ServletContextEvent event) {
//        HibernateUtil.getSessionFactory().close(); // Free all resources
        SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
        //Ovaj dio dodan da bi se preventiralo zaglavljivanje Tomcata
        //Dohvaća se direktno C3P0 konekcija kako bi se zatvorila
        //Do sada se zatvarao samo session factory od Hiberneta koji je onda posredno zatvarao C3P0 konekciju
        if(sesionFactory instanceof SessionFactoryImpl) {
            SessionFactoryImpl sf = (SessionFactoryImpl)sesionFactory;
            ConnectionProvider conn = sf.getConnectionProvider();
            if(conn instanceof C3P0ConnectionProvider) { 
              ((C3P0ConnectionProvider)conn).close(); 
            }
         }
        sesionFactory.close();
    }
}
