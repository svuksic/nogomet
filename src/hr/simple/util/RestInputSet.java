package hr.simple.util;


public class RestInputSet {

	private String genericValue;
	//for CRUD scroll
	private Integer startPosition;
	private Integer maxPosition;

	public RestInputSet() {
		super();
	}
	
	public Integer getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Integer startPosition) {
		this.startPosition = startPosition;
	}

	public Integer getMaxPosition() {
		return maxPosition;
	}

	public void setMaxPosition(Integer maxPosition) {
		this.maxPosition = maxPosition;
	}

	public RestInputSet(String genericValue) {
		super();
		this.genericValue = genericValue;
	}

	public String getGenericValue() {
		return genericValue;
	}

	public void setGenericValue(String genericValue) {
		this.genericValue = genericValue;
	}

}
