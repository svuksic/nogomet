package hr.simple.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SimpleUtil {
	
	/**
	 * Metoda za pretvaranje tekstualnog fajla u string (java 7)
	 * Nije pametno za velike fajlove ali u ovom slušaju še se koristiti samo za dohvat stranica što i nije strašno
	 * barem ne za sad
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String path, Charset encoding) throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}
	
	/**
	 * Metoda koja sprema fajl na zadanu putanju
	 * @param path
	 * @param uploadedInputStream
	 * @throws IOException
	 */
	public static void saveFile(String path, InputStream uploadedInputStream) throws IOException{
		try{
			int read = 0;
			byte[] bytes = new byte[1024];
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}else{
				Date now = new Date();
				file.setLastModified(now.getTime());
			}
			FileOutputStream outpuStream = new FileOutputStream(file);			
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {			
			throw e;
		}
	}
	
	/**
	 * Provjerava postoji li datoteka
	 * @param path
	 * @return
	 */
	public static boolean existFile(String path){
		boolean result = false;
		File file = new File(path);
		if (file.exists()) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Metoda za provjeru je li string numeriški
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    @SuppressWarnings("unused")
		double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;
	  }  
	  return true;  
	}
	
	/**
	 * Vraca danasnji datum i vrijeme
	 */
	public static String getCurrentDateTime(){
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	
	/**
	 * formatiranje datuma u zeljeni string format
	 * @param datum
	 * @return
	 */
	public static String formateDatum(Date datum){
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");		
		return dateFormat.format(datum);
	}
	
	/**
	 * 
	 * @param datum
	 * @return
	 */
	public static String formateDateOnly(Date datum){
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");		
		return dateFormat.format(datum);
	}
	
	/**
	 * 
	 * @param datum
	 * @return
	 */
	public static String formateTimeOnly(Date datum){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");		
		return dateFormat.format(datum);
	}
	
	public static String formateTimeOnlyWithoutSec(Date datum){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");		
		return dateFormat.format(datum);
	}
	
	/**
	 * create date from string
	 * @param date
	 * @return 
	 */
	public static Date createDate(String date){
		Date result;
		try{
			result = new SimpleDateFormat("dd.MM.yyy").parse(date);
		}catch(Exception ex){
			result = null;
		}
		return result;
	}
	
	public static Date createDateTime(String date){
		Date result;
		try{
			result = new SimpleDateFormat("dd.MM.yyy hh:mm").parse(date);
		}catch(Exception ex){
			result = null;
		}
		return result;
	}
	
	/**
	 * Double or long return in 2 decimal places format (#.00)
	 * @param value
	 * @return
	 */
	public static String getDoubleDecimalPlacesFormat(Object value){
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(value).replace(",", ".");
	}
	
	/**
	 * Na datum dodaje dane ili oduzima ako damo negativan iznos
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
	
	/**
	 * Na vrijeme dodaje minute
	 * @param date
	 * @param minutes
	 * @return
	 */
	public static Date addMinutes(Date date, int minutes)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }
	
//	public static void addLogToDatabase(Long userId, Long groupId, String message, Session session){		
//		try{
//			Log logDb = new Log();
//			logDb.setDate(new Date());
//			GrupaKorisnika group = (GrupaKorisnika) session.get(GrupaKorisnika.class, groupId);
//			logDb.setGroup(group);
//			logDb.setMessage(message);
//			User user = (User) session.get(User.class, userId);
//			logDb.setUser(user);	
//			session.save(logDb);
//		}catch(Exception ex){
//			ex.printStackTrace();
//			System.out.println("Greška kod pokušaja log-a.");
//			System.out.println(ex.toString());
//		}		
//	}

}
