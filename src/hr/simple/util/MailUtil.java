package hr.simple.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailUtil extends Thread{
	
	private String fromAddress;
	private String toAddress;
	private String subject;
	private String msgBody;
	
	public MailUtil(String fromAddress, String toAddress, String subject, String msgBody) {
		super();
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.subject = subject;
		this.msgBody = msgBody;
	}
	
	 public void run(){
	       sendMail();
	 }

	 private void sendMail(){

		 // Recipient's email ID needs to be mentioned.
		 String to = toAddress;

		 // Sender's email ID needs to be mentioned
		 String from = fromAddress;

		 // Get system properties
		 Properties properties = System.getProperties();

		 final String username = "tkoigra.com@gmail.com";
		 final String password = "svuksic712150";

		 properties.put("mail.smtp.auth", "true");
		 properties.put("mail.smtp.starttls.enable", "true");
		 properties.put("mail.smtp.host", "smtp.gmail.com");
		 properties.put("mail.smtp.port", "587");
		 properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		 Session session = Session.getInstance(properties,
				 new javax.mail.Authenticator() {
			 protected PasswordAuthentication getPasswordAuthentication() {
				 return new PasswordAuthentication(username, password);
			 }
		 });

		 try{
			 // Create a default MimeMessage object.
			 MimeMessage message = new MimeMessage(session);

			 // Set From: header field of the header.
			 message.setFrom(new InternetAddress(from));

			 // Set To: header field of the header.
			 message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			 // Set Subject: header field
			 message.setSubject(subject);

			 MimeMultipart masterPart = new MimeMultipart();  
			 MimeBodyPart bodyPart   = new MimeBodyPart();    

			 bodyPart.setContent(msgBody, "text/html; charset=utf-8");  
			 masterPart.addBodyPart(bodyPart); 
			 // Send the actual HTML message, as big as you like
			 message.setContent(masterPart);

			 // Send message
			 Transport.send(message);
			 System.out.println("Sent message successfully....");
		 }catch (Exception ex) {
			 ex.printStackTrace();
		 }
	}


}
