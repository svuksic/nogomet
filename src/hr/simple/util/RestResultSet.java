package hr.simple.util;

import com.google.gson.Gson;

public class RestResultSet {

	protected String returnMessage = null;
	protected String errorMessage = null;
	
	Gson gson = null;
	
	public RestResultSet() {
		super();
		gson = new Gson();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}	

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}
	
	public String toJson(){
		return gson.toJson(this);
	}
}
