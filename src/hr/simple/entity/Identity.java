package hr.simple.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="identity")
public class Identity {

	@Id
	@GeneratedValue
	@Column(name="idIdentity")
	private Integer indentittyId;
	@Column(name="provider_user_id")
	private String identityUserId;
	@Column(name="provider")
	private String provider;
	@Column(name="merged")
	private String merged;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
	private User user;
	
	public Integer getIndentittyId() {
		return indentittyId;
	}
	public void setIndentittyId(Integer indentittyId) {
		this.indentittyId = indentittyId;
	}
	public String getIdentityUserId() {
		return identityUserId;
	}
	public void setIdentityUserId(String identityUserId) {
		this.identityUserId = identityUserId;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getMerged() {
		return merged;
	}
	public void setMerged(String merged) {
		this.merged = merged;
	}
	
	
}
