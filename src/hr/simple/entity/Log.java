package hr.simple.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="log")
public class Log {

	@Id
    @GeneratedValue
    @Column(name="idlog")
	private Long idLog;
	@Column(name="message")
    private String message;
	@Column(name="insertDate")
	private Date date;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user")
    private User user;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="userGroup")
    private GrupaKorisnika group;
	
	public Long getIdLog() {
		return idLog;
	}
	public void setIdLog(Long idLog) {
		this.idLog = idLog;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public GrupaKorisnika getGroup() {
		return group;
	}
	public void setGroup(GrupaKorisnika group) {
		this.group = group;
	}
	
}
