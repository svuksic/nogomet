package hr.simple.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="prometi")
public class Promet {
	
	@Id
    @GeneratedValue
    @Column(name="idPrometi")
	private Long idPromet;
	@Column(name="iznos")
	private Double iznos;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="blagajnik")
	private User blagajnik;
	
	public Long getIdPromet() {
		return idPromet;
	}
	public void setIdPromet(Long idPromet) {
		this.idPromet = idPromet;
	}
	public Double getIznos() {
		return iznos;
	}
	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public User getBlagajnik() {
		return blagajnik;
	}
	public void setBlagajnik(User blagajnik) {
		this.blagajnik = blagajnik;
	}
	
}
