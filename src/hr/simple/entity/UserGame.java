package hr.simple.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_games")
public class UserGame implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String ULOGA_USER_BLAGAJNIK = "Igrač/Blagajnik";
	public static final String ULOGA_USER_IGRAC = "Igrač";
	
	@Id
	@GeneratedValue
	@Column(name="id_user_game")
	private Long idUserGame;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="iduser")
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idgame")
	private Game game;
	@Column(name="user_uloga")
	private String userUloga;	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="promet")
	private Promet promet;
		
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public String getUserUloga() {
		return userUloga;
	}
	public void setUserUloga(String userUloga) {
		this.userUloga = userUloga;
	}
	public Long getIdUserGame() {
		return idUserGame;
	}
	public void setIdUserGame(Long idUserGame) {
		this.idUserGame = idUserGame;
	}
	public Promet getPromet() {
		return promet;
	}
	public void setPromet(Promet promet) {
		this.promet = promet;
	}
	
}
