package hr.simple.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grupakorisnika")
public class GrupaKorisnika {
	

	@Id
    @GeneratedValue
    @Column(name="id_gk")
	private Long idGk;
	@Column(name="naziv_gk")
    private String nazivGk;
	@Column(name="opis_gk")
    private String opisGk;

	public GrupaKorisnika(){
	}
	
	public Long getIdGk() {
		return idGk;
	}
	public void setIdGk(Long idGk) {
		this.idGk = idGk;
	}
	public String getNazivGk() {
		return nazivGk;
	}
	public void setNazivGk(String nazivGk) {
		this.nazivGk = nazivGk;
	}
	public String getOpisGk() {
		return opisGk;
	}
	public void setOpisGk(String opisGk) {
		this.opisGk = opisGk;
	}
}
