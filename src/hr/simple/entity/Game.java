package hr.simple.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="games")
public class Game {

	@Id
	@GeneratedValue
	@Column(name="idGames")
	private Long idGames;
	@Column(name="startTime")
	private Date startTime;
	@Column(name="location")
	private String location;
	@Column(name="gameType")
	private String gameType;
	@Column(name="price")
	private Double price;
	@Column(name="sifra")
	private String sifra;	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="blagajnik")
	private User blagajnik;
	
	public Long getIdGames() {
		return idGames;
	}
	public void setIdGames(Long idGames) {
		this.idGames = idGames;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getGameType() {
		return gameType;
	}
	public void setGameType(String gameType) {
		this.gameType = gameType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public User getBlagajnik() {
		return blagajnik;
	}
	public void setBlagajnik(User blagajnik) {
		this.blagajnik = blagajnik;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	
}