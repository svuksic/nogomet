package hr.simple.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="user")
public class User {
	
	@Id
    @GeneratedValue
    @Column(name="iduser")
    private Long idUser;
	@Column(name="ime")
    private String ime;
	@Column(name="prezime")
    private String prezime;
	@Column(name="ussername")
    private String ussername;
	@Column(name="password")
    private String password;
	@Column(name="token")
    private String token;
	@Column(name="oib")
    private String oib;
	@Column(name="active_user")
    private String activeUser;
	@Column(name="last_wrong_login")
    private Date lastWrongLogin;
	@Column(name="number_wrong_login")
    private Integer numberWrongLogin;
	@Column(name="send_mail")
	private String sendMail;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="grupa")
    private GrupaKorisnika grupa;
	
	public User(Long idUser, String ime, String prezime, String ussername,
			String password, String token) {
		super();
		this.idUser = idUser;
		this.ime = ime;
		this.prezime = prezime;
		this.ussername = ussername;
		this.password = password;
		this.token = token;
	}
	
	public User(){
		
	}
	
	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getUssername() {
		return ussername;
	}
	public void setUssername(String ussername) {
		this.ussername = ussername;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public GrupaKorisnika getGrupa() {
		return grupa;
	}

	public void setGrupa(GrupaKorisnika grupa) {
		this.grupa = grupa;
	}

	public Date getLastWrongLogin() {
		return lastWrongLogin;
	}

	public void setLastWrongLogin(Date lastWrongLogin) {
		this.lastWrongLogin = lastWrongLogin;
	}

	public Integer getNumberWrongLogin() {
		return numberWrongLogin;
	}

	public void setNumberWrongLogin(Integer numberWrongLogin) {
		this.numberWrongLogin = numberWrongLogin;
	}

	public String getSendMail() {
		return sendMail;
	}

	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}

	public String getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(String activeUser) {
		this.activeUser = activeUser;
	}
	
}
