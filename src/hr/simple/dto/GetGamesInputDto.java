package hr.simple.dto;

import hr.simple.util.RestInputSet;

public class GetGamesInputDto extends RestInputSet{

	private String activeFilter;

	public String getActiveFilter() {
		return activeFilter;
	}

	public void setActiveFilter(String activeFilter) {
		this.activeFilter = activeFilter;
	}
	
	
}
