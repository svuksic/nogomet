package hr.simple.dto;

public class InsertGame {

	private String vrstaUtakmice;
	private String lokacija;
	private String cijenaTerena;
	private String danGame;
	private String mjesecGame;
	private String godinaGame;
	private String satGame;
	private String minGame;
	private String blagajnik;
	private String blagajnikId;
	private String sifra;
	
	public String getVrstaUtakmice() {
		return vrstaUtakmice;
	}
	public void setVrstaUtakmice(String vrstaUtakmice) {
		this.vrstaUtakmice = vrstaUtakmice;
	}
	public String getLokacija() {
		return lokacija;
	}
	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}
	public String getCijenaTerena() {
		return cijenaTerena;
	}
	public void setCijenaTerena(String cijenaTerena) {
		this.cijenaTerena = cijenaTerena;
	}
	public String getDanGame() {
		return danGame;
	}
	public void setDanGame(String danGame) {
		this.danGame = danGame;
	}
	public String getMjesecGame() {
		return mjesecGame;
	}
	public void setMjesecGame(String mjesecGame) {
		this.mjesecGame = mjesecGame;
	}
	public String getGodinaGame() {
		return godinaGame;
	}
	public void setGodinaGame(String godinaGame) {
		this.godinaGame = godinaGame;
	}
	public String getSatGame() {
		return satGame;
	}
	public void setSatGame(String satGame) {
		this.satGame = satGame;
	}
	public String getMinGame() {
		return minGame;
	}
	public void setMinGame(String minGame) {
		this.minGame = minGame;
	}
	public String getBlagajnik() {
		return blagajnik;
	}
	public void setBlagajnik(String blagajnik) {
		this.blagajnik = blagajnik;
	}
	public String getBlagajnikId() {
		return blagajnikId;
	}
	public void setBlagajnikId(String blagajnikId) {
		this.blagajnikId = blagajnikId;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}	
	
}
