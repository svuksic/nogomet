package hr.simple.dto;

import hr.simple.util.RestInputSet;

public class RegistracijaInputDto extends RestInputSet{
	
	private String usernameReg;
	private String passwordReg;
	private String ime;
	private String prezime;
	
	public String getUsername() {
		return usernameReg;
	}
	public void setUsername(String username) {
		this.usernameReg = username;
	}
	public String getPassword() {
		return passwordReg;
	}
	public void setPassword(String password) {
		this.passwordReg = password;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

}