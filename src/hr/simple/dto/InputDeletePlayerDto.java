package hr.simple.dto;

public class InputDeletePlayerDto {
	private String logedUserId;
	private String selectedGameId;
	
	public String getLogedUserId() {
		return logedUserId;
	}
	public void setLogedUserId(String logedUserId) {
		this.logedUserId = logedUserId;
	}
	public String getSelectedGameId() {
		return selectedGameId;
	}
	public void setSelectedGameId(String selectedGameId) {
		this.selectedGameId = selectedGameId;
	}
}
