package hr.simple.dto;

public class InputAddUserToGameDto {
	
	private String logedUserId;
	private String selectedGameId;
	private String sifra;
	
	public String getLogedUserId() {
		return logedUserId;
	}
	public void setLogedUserId(String logedUserId) {
		this.logedUserId = logedUserId;
	}
	public String getSelectedGameId() {
		return selectedGameId;
	}
	public void setSelectedGameId(String selectedGameId) {
		this.selectedGameId = selectedGameId;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	
	
}
