package hr.simple.dto;

import hr.simple.util.RestResultSet;

public class UserDto extends RestResultSet{

	private Long sifra;
	private String ussername;
	private String password;
	private Long grupa;
	private String ime;
	private String prezime;
	private String token;
	private String oib;
	private String existAvatar;
	private String idUser;
	private String isMerged;
	private String sendMail;
	
	public String getOib() {
		return oib;
	}
	public void setOib(String oib) {
		this.oib = oib;
	}
	public Long getSifra() {
		return sifra;
	}
	public void setSifra(Long sifra) {
		this.sifra = sifra;
	}
	public String getUsername() {
		return ussername;
	}
	public void setUsername(String username) {
		this.ussername = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getGrupa() {
		return grupa;
	}
	public void setGrupa(Long grupa) {
		this.grupa = grupa;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getExistAvatar() {
		return existAvatar;
	}
	public void setExistAvatar(String existAvatar) {
		this.existAvatar = existAvatar;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getIsMerged() {
		return isMerged;
	}
	public void setIsMerged(String isMerged) {
		this.isMerged = isMerged;
	}
	public String getSendMail() {
		return sendMail;
	}
	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}
	
	
	
}
