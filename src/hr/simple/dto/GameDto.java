package hr.simple.dto;

import hr.simple.util.RestResultSet;

import java.util.ArrayList;
import java.util.List;

public class GameDto extends RestResultSet{
	
	private String idUtakmice;
	private String lokacijaUtakmice;
	private String tipUtakmice;
	private String vrijemeIgranja;
	private String datumIgranja;
	private String cijenaUtakmice;
	private String cijenaUtakmicePoIgracu;
	private String hideRemoveBtn;
	private String sifra;
	private List<GamePlayerDto> igraciUtakmice = new ArrayList<GamePlayerDto>();
	
	public String getIdUtakmice() {
		return idUtakmice;
	}
	public void setIdUtakmice(String idUtakmice) {
		this.idUtakmice = idUtakmice;
	}
	public String getLokacijaUtakmice() {
		return lokacijaUtakmice;
	}
	public void setLokacijaUtakmice(String lokacijaUtakmice) {
		this.lokacijaUtakmice = lokacijaUtakmice;
	}
	public String getTipUtakmice() {
		return tipUtakmice;
	}
	public void setTipUtakmice(String tipUtakmice) {
		this.tipUtakmice = tipUtakmice;
	}
	public String getVrijemeIgranja() {
		return vrijemeIgranja;
	}
	public void setVrijemeIgranja(String vrijemeIgranja) {
		this.vrijemeIgranja = vrijemeIgranja;
	}
	public String getDatumIgranja() {
		return datumIgranja;
	}
	public void setDatumIgranja(String datumIgranja) {
		this.datumIgranja = datumIgranja;
	}
	public String getCijenaUtakmice() {
		return cijenaUtakmice;
	}
	public void setCijenaUtakmice(String cijenaUtakmice) {
		this.cijenaUtakmice = cijenaUtakmice;
	}
	public List<GamePlayerDto> getIgraciUtakmice() {
		return igraciUtakmice;
	}
	public void setIgraciUtakmice(List<GamePlayerDto> igraciUtakmice) {
		this.igraciUtakmice = igraciUtakmice;
	}

	public void addIgrac(GamePlayerDto igrac){
		this.igraciUtakmice.add(igrac);
	}
	public String getCijenaUtakmicePoIgracu() {
		return cijenaUtakmicePoIgracu;
	}
	public void setCijenaUtakmicePoIgracu(String cijenaUtakmicePoIgracu) {
		this.cijenaUtakmicePoIgracu = cijenaUtakmicePoIgracu;
	}
	public String getHideRemoveBtn() {
		return hideRemoveBtn;
	}
	public void setHideRemoveBtn(String hideRemoveBtn) {
		this.hideRemoveBtn = hideRemoveBtn;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	
	
}
