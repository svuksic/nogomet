package hr.simple.dto;

public class InsertUser {
	
	private String usernameU;
	private String passwordU;
	private Long grupaU;
	private String imeU;
	private String prezimeU;
	private String nazivGk;
	
	public String getUsernameU() {
		return usernameU;
	}
	public void setUsernameU(String usernameU) {
		this.usernameU = usernameU;
	}
	public String getPasswordU() {
		return passwordU;
	}
	public void setPasswordU(String passwordU) {
		this.passwordU = passwordU;
	}
	public Long getGrupaU() {
		return grupaU;
	}
	public void setGrupaU(Long grupaU) {
		this.grupaU = grupaU;
	}
	public String getImeU() {
		return imeU;
	}
	public void setImeU(String imeU) {
		this.imeU = imeU;
	}
	public String getPrezimeU() {
		return prezimeU;
	}
	public void setPrezimeU(String prezimeU) {
		this.prezimeU = prezimeU;
	}
	public String getNazivGk() {
		return nazivGk;
	}
	public void setNazivGk(String nazivGk) {
		this.nazivGk = nazivGk;
	}
    
	
}
