package hr.simple.dto;

public class GamePlayerDto {

	private String userId;
	private String ime;
	private String prezime;
	private String uloga;
	private String hideRemoveBtn;
	private String existAvatar;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getUloga() {
		return uloga;
	}
	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	public String getHideRemoveBtn() {
		return hideRemoveBtn;
	}
	public void setHideRemoveBtn(String hideRemoveBtn) {
		this.hideRemoveBtn = hideRemoveBtn;
	}
	public String getExistAvatar() {
		return existAvatar;
	}
	public void setExistAvatar(String existAvatar) {
		this.existAvatar = existAvatar;
	}
	
	
}
