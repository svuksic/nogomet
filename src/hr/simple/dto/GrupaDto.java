package hr.simple.dto;

import hr.simple.util.RestResultSet;


public class GrupaDto extends RestResultSet{
	
	private Long idGk;
    private String nazivGk;
    private String opisGk;
    private String ulicaGk;
    private String kucniBrojGk;
    private String mjestoGk;
    private String telGk;
    private String rvodGk;
    private String rvdoGk;
    private String vlasnikGk;
    private String oibGk;
    private String sustavPdvGk;
    private String pocetniBrojRacunaGk;
    private String sifraCertifikatGk;
    private String fiskalServiceUrl;
    private String podnozjeRacuna;
    private String velicinaRacuna;
    private String r1r2;
    private String dodatniPodaci;
    private String odgodaPlacanja;
    
	public GrupaDto() {
		super();
	}

	public String getVlasnikGk() {
		return vlasnikGk;
	}


	public void setVlasnikGk(String vlasnikGk) {
		this.vlasnikGk = vlasnikGk;
	}


	public String getOibGk() {
		return oibGk;
	}


	public void setOibGk(String oibGk) {
		this.oibGk = oibGk;
	}


	public String getTelGk() {
		return telGk;
	}

	public void setTelGk(String telGk) {
		this.telGk = telGk;
	}


	public Long getIdGk() {
		return idGk;
	}
	public void setIdGk(Long idGk) {
		this.idGk = idGk;
	}
	public String getNazivGk() {
		return nazivGk;
	}
	public void setNazivGk(String nazivGk) {
		this.nazivGk = nazivGk;
	}
	public String getOpisGk() {
		return opisGk;
	}
	public void setOpisGk(String opisGk) {
		this.opisGk = opisGk;
	}
	public String getRvodGk() {
		return rvodGk;
	}
	public void setRvodGk(String rvodGk) {
		this.rvodGk = rvodGk;
	}
	public String getRvdoGk() {
		return rvdoGk;
	}
	public void setRvdoGk(String rvdoGk) {
		this.rvdoGk = rvdoGk;
	}

	public String getUlicaGk() {
		return ulicaGk;
	}

	public void setUlicaGk(String ulicaGk) {
		this.ulicaGk = ulicaGk;
	}

	public String getKucniBrojGk() {
		return kucniBrojGk;
	}

	public void setKucniBrojGk(String kucniBrojGk) {
		this.kucniBrojGk = kucniBrojGk;
	}

	public String getMjestoGk() {
		return mjestoGk;
	}

	public void setMjestoGk(String mjestoGk) {
		this.mjestoGk = mjestoGk;
	}

	public String getPocetniBrojRacuna() {
		return pocetniBrojRacunaGk;
	}

	public void setPocetniBrojRacuna(String pocetniBrojRacuna) {
		this.pocetniBrojRacunaGk = pocetniBrojRacuna;
	}

	public String getPocetniBrojRacunaGk() {
		return pocetniBrojRacunaGk;
	}

	public void setPocetniBrojRacunaGk(String pocetniBrojRacunaGk) {
		this.pocetniBrojRacunaGk = pocetniBrojRacunaGk;
	}

	public String getSifraCertifikatGk() {
		return sifraCertifikatGk;
	}

	public void setSifraCertifikatGk(String sifraCertifikatGk) {
		this.sifraCertifikatGk = sifraCertifikatGk;
	}

	public String getFiskalServiceUrl() {
		return fiskalServiceUrl;
	}

	public void setFiskalServiceUrl(String fiskalServiceUrl) {
		this.fiskalServiceUrl = fiskalServiceUrl;
	}

	public String getPodnozjeRacuna() {
		return podnozjeRacuna;
	}

	public void setPodnozjeRacuna(String podnozjeRacuna) {
		this.podnozjeRacuna = podnozjeRacuna;
	}

	public String getSustavPdvGk() {
		return sustavPdvGk;
	}

	public void setSustavPdvGk(String sustavPdvGk) {
		this.sustavPdvGk = sustavPdvGk;
	}

	public String getVelicinaRacuna() {
		return velicinaRacuna;
	}

	public void setVelicinaRacuna(String velicinaRacuna) {
		this.velicinaRacuna = velicinaRacuna;
	}

	public String getR1r2() {
		return r1r2;
	}

	public void setR1r2(String r1r2) {
		this.r1r2 = r1r2;
	}

	public String getDodatniPodaci() {
		return dodatniPodaci;
	}

	public void setDodatniPodaci(String dodatniPodaci) {
		this.dodatniPodaci = dodatniPodaci;
	}

	public String getOdgodaPlacanja() {
		return odgodaPlacanja;
	}

	public void setOdgodaPlacanja(String odgodaPlacanja) {
		this.odgodaPlacanja = odgodaPlacanja;
	}
	
}
