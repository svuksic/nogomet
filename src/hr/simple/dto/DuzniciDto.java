package hr.simple.dto;

import java.util.ArrayList;
import java.util.List;

import hr.simple.util.RestResultSet;

public class DuzniciDto extends RestResultSet{

	List<DuznikDto> listaDuznika = new ArrayList<DuznikDto>();

	public List<DuznikDto> getListaDuznika() {
		return listaDuznika;
	}

	public void setListaDuznika(List<DuznikDto> listaDuznika) {
		this.listaDuznika = listaDuznika;
	}
	
	public void addDuznik(DuznikDto duznik){
		this.listaDuznika.add(duznik);
	}
}
