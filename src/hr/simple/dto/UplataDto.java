package hr.simple.dto;

import hr.simple.util.RestInputSet;

public class UplataDto extends RestInputSet{

	//Id korisnika za kojeg se uplaćuje
	private String idKorisnika;
	private String iznosUplate;
	
	public String getIdKorisnika() {
		return idKorisnika;
	}
	public void setIdKorisnika(String idKorisnika) {
		this.idKorisnika = idKorisnika;
	}
	public String getIznosUplate() {
		return iznosUplate;
	}
	public void setIznosUplate(String iznosUplate) {
		this.iznosUplate = iznosUplate;
	}
	
	
}
