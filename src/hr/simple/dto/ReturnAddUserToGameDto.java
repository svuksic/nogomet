package hr.simple.dto;

import java.util.ArrayList;
import java.util.List;

import hr.simple.util.RestResultSet;

public class ReturnAddUserToGameDto extends RestResultSet{
	
	private List<GamePlayerDto> igraciUtakmice = new ArrayList<GamePlayerDto>();
	private String hideRemoveBtn;
	private GameDto gameData;
	private String addedUserId;

	public List<GamePlayerDto> getIgraciUtakmice() {
		return igraciUtakmice;
	}

	public void setIgraciUtakmice(List<GamePlayerDto> igraciUtakmice) {
		this.igraciUtakmice = igraciUtakmice;
	}
	
	public void addPlayerToGame(GamePlayerDto player){
		this.igraciUtakmice.add(player);
	}

	public String getHideRemoveBtn() {
		return hideRemoveBtn;
	}

	public void setHideRemoveBtn(String hideRemoveBtn) {
		this.hideRemoveBtn = hideRemoveBtn;
	}

	public GameDto getGameData() {
		return gameData;
	}

	public void setGameData(GameDto gameData) {
		this.gameData = gameData;
	}

	public String getAddedUserId() {
		return addedUserId;
	}

	public void setAddedUserId(String addedUserId) {
		this.addedUserId = addedUserId;
	}
	
	
}
