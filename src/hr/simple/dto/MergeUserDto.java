package hr.simple.dto;

public class MergeUserDto {
	
	private String mergeUssername;
	private String mergePassword;
	
	public String getUssername() {
		return mergeUssername;
	}
	public void setUssername(String ussername) {
		this.mergeUssername = ussername;
	}
	public String getPassword() {
		return mergePassword;
	}
	public void setPassword(String password) {
		this.mergePassword = password;
	}

	
}
