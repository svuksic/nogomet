package hr.simple.dto;

import hr.simple.util.RestResultSet;

public class MergeUserReturnDto extends RestResultSet{

	private String mergeUserId;
	private String mergeGroupId;

	public String getMergeUserId() {
		return mergeUserId;
	}

	public void setMergeUserId(String mergeUserId) {
		this.mergeUserId = mergeUserId;
	}

	public String getMergeGroupId() {
		return mergeGroupId;
	}

	public void setMergeGroupId(String mergeGroupId) {
		this.mergeGroupId = mergeGroupId;
	}
	
	
}
