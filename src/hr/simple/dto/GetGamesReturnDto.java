package hr.simple.dto;

import hr.simple.util.RestResultSet;

import java.util.ArrayList;
import java.util.List;

public class GetGamesReturnDto extends RestResultSet{

	private List<GameDto> gamesList = new ArrayList<GameDto>();

	public List<GameDto> getGamesList() {
		return gamesList;
	}

	public void setGamesList(List<GameDto> gamesList) {
		this.gamesList = gamesList;
	}
	
	public void addGameToList(GameDto game){
		this.gamesList.add(game);
	}

	
}
