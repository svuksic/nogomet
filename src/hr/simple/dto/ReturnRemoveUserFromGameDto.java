package hr.simple.dto;

import hr.simple.util.RestResultSet;

import java.util.ArrayList;
import java.util.List;

public class ReturnRemoveUserFromGameDto extends RestResultSet{

	private List<GamePlayerDto> igraciUtakmice = new ArrayList<GamePlayerDto>();
	private String hideRemoveBtn;
	private GameDto gameData;

	public List<GamePlayerDto> getIgraciUtakmice() {
		return igraciUtakmice;
	}

	public void setIgraciUtakmice(List<GamePlayerDto> igraciUtakmice) {
		this.igraciUtakmice = igraciUtakmice;
	}
	
	public void addPlayerToGame(GamePlayerDto player){
		this.igraciUtakmice.add(player);
	}

	public String getHideRemoveBtn() {
		return hideRemoveBtn;
	}

	public void setHideRemoveBtn(String hideRemoveBtn) {
		this.hideRemoveBtn = hideRemoveBtn;
	}

	public GameDto getGameData() {
		return gameData;
	}

	public void setGameData(GameDto gameData) {
		this.gameData = gameData;
	}
	
}
