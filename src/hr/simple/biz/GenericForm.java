package hr.simple.biz;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

public interface GenericForm {

	@POST
	@Path("/getAllItems")	
	public String getAllItems(String firstRow, @HeaderParam("grupa") Long grupa);
	@POST
	@Path("/insertItem")
	public String insertItem(String item, @HeaderParam("grupa") Long grupa);
	@POST
	@Path("/editItem")
	public String editItem(String item, @HeaderParam("grupa") Long grupa);
	@POST
	@Path("/deleteItem")
	public String deleteItem(String idItem, @HeaderParam("grupa") Long grupa);
	@POST
	@Path("/searchItem")
	public String searchItem(String item, @HeaderParam("grupa") Long grupa);
	@POST
	@Path("/getItem")
	public String getItem(String idItem, @HeaderParam("grupa") Long grupa);
}
