package hr.simple.biz;

import hr.simple.dto.DuzniciDto;
import hr.simple.dto.DuznikDto;
import hr.simple.dto.GameDto;
import hr.simple.dto.GamePlayerDto;
import hr.simple.dto.GetGamesInputDto;
import hr.simple.dto.GetGamesReturnDto;
import hr.simple.dto.InputAddUserToGameDto;
import hr.simple.dto.InputDeletePlayerDto;
import hr.simple.dto.InsertGame;
import hr.simple.dto.ReturnAddUserToGameDto;
import hr.simple.dto.ReturnRemoveUserFromGameDto;
import hr.simple.dto.UplataDto;
import hr.simple.entity.Game;
import hr.simple.entity.Promet;
import hr.simple.entity.User;
import hr.simple.entity.UserGame;
import hr.simple.facade.Naplata;
import hr.simple.hibernate.HibernateUtil;
import hr.simple.util.MailUtil;
import hr.simple.util.RestResultSet;
import hr.simple.util.SimpleUtil;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gson.Gson;

@Path("/gameHandler")
public class GameRest {
	
	@Context ServletContext context;

	@POST
	@Path("/insertGame")
	public String insertGame(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		InsertGame input = gson.fromJson(data, InsertGame.class);
		RestResultSet result = new RestResultSet();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Game game = new Game();
			Date startDate = SimpleUtil.createDateTime(input.getDanGame()+"."+input.getMjesecGame()+"."+input.getGodinaGame()+" "+input.getSatGame()+":"+input.getMinGame());
			game.setStartTime(startDate);
			game.setGameType(input.getVrstaUtakmice());
			game.setLocation(input.getLokacija());			
			//Ako se utakmica placa
			if(input.getBlagajnikId() != null && !input.getBlagajnikId().isEmpty()){
				User blagajnik = (User)session.get(User.class, new Long(input.getBlagajnikId()));
				game.setBlagajnik(blagajnik);
				if(input.getCijenaTerena() != null && !input.getCijenaTerena().isEmpty()){
					game.setPrice(new Double(input.getCijenaTerena()));
				}
			}
			if(input.getSifra() != null && !input.getSifra().isEmpty()){
				game.setSifra(input.getSifra());
			}
			session.persist(game);
			//Automatski onaj tko kreira utakmicu postaje igrač
			User user = (User) session.get(User.class, userId);			
			UserGame userGame = new UserGame();
			//Postavljamo ulogu korisnika
			if(input.getBlagajnikId() != null && !input.getBlagajnikId().isEmpty()){
				if(user.getIdUser() == game.getBlagajnik().getIdUser()){
					userGame.setUserUloga(UserGame.ULOGA_USER_BLAGAJNIK);
				}else{
					userGame.setUserUloga(UserGame.ULOGA_USER_IGRAC);
				}
			}else{
				userGame.setUserUloga(UserGame.ULOGA_USER_IGRAC);
			}
			userGame.setGame(game);
			userGame.setUser(user);
			//Kreiramo dugovni promet za kreatora utakmice
			Naplata naplata = new Naplata();
			Promet promet = new Promet();
			promet.setBlagajnik(game.getBlagajnik());
			Double iznos = naplata.calculatePricePerPlayer(game, session);
			if(iznos != null)
				promet.setIznos(-iznos);
			promet.setUser(user);
			session.persist(promet);
			userGame.setPromet(promet);
			session.persist(userGame);			
			result.setReturnMessage("Uspješno kreirana utakmica.");
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod kreiranje utakmice.");
		}
		return result.toJson();
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/getGames")
	public String getGames(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		Naplata naplata = new Naplata();
		GetGamesInputDto input = gson.fromJson(data, GetGamesInputDto.class);
		GetGamesReturnDto result = new GetGamesReturnDto();
		Transaction tr = null;
		Session session = null;
		try {			
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			User user = (User) session.get(User.class, userId);			
			if(input.getActiveFilter().equals("mojeUtakmice")){
				Query query = session.createQuery("select ug from UserGame ug"
						+ " right join fetch ug.game g "
						+ " inner join fetch ug.user u "
						+ " where u.idUser = :idUser "
						+ " order by g.startTime desc ");
				query.setParameter("idUser", user.getIdUser());
				query.setFirstResult(input.getStartPosition());
				query.setMaxResults(input.getMaxPosition());
				List<UserGame> listGames = query.list();
				for(UserGame game : listGames){
					Boolean isMygame = false;
					GameDto gamesDto = new GameDto();
					if(game.getGame().getPrice() != null)
						gamesDto.setCijenaUtakmice(game.getGame().getPrice().toString());
					gamesDto.setDatumIgranja(SimpleUtil.formateDateOnly(game.getGame().getStartTime()));
					gamesDto.setIdUtakmice(game.getGame().getIdGames().toString());
					gamesDto.setLokacijaUtakmice(game.getGame().getLocation().toString());
					gamesDto.setTipUtakmice(game.getGame().getGameType().toString());
					gamesDto.setVrijemeIgranja(SimpleUtil.formateTimeOnlyWithoutSec(game.getGame().getStartTime()));
					if(game.getGame().getSifra() != null)
						gamesDto.setSifra("true");
					if(game.getGame().getPrice() != null)
						gamesDto.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game.getGame(), session).toString());
					//
					//Dodavanje igrača po utakmici
					Query queryPlayers = session.createQuery("select ug from UserGame ug"
							+ " inner join fetch ug.user u "
							+ " inner join fetch ug.game g "
							+ " where g.idGames = :gameId "
							+ " order by ug.idUserGame ");
					queryPlayers.setParameter("gameId", game.getGame().getIdGames());
					List<UserGame> playersList = queryPlayers.list();
					for(UserGame u : playersList){
						GamePlayerDto playerDto = new GamePlayerDto();
						playerDto.setUserId(u.getUser().getIdUser().toString());
						playerDto.setIme(u.getUser().getIme());
						playerDto.setPrezime(u.getUser().getPrezime());
						playerDto.setUloga(u.getUserUloga());
						if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
							playerDto.setExistAvatar("true");
						if(userId.equals(u.getUser().getIdUser())){
							isMygame = true;
						}
						gamesDto.addIgrac(playerDto);
					}
					if(!isMygame)
						gamesDto.setHideRemoveBtn("nogometInvisible");
					result.addGameToList(gamesDto);
				}
			}else if(input.getActiveFilter().equals("danasnjeUtakmice")){
				Query query = session.createQuery("select g from Game g"
						+ " where g.startTime > :minToday "
						+ " and g.startTime < :maxToday "
						+ " order by g.startTime desc ");
				query.setParameter("minToday", SimpleUtil.createDateTime((SimpleUtil.formateDateOnly(new Date())+" 00:00:00")));
				query.setParameter("maxToday", SimpleUtil.createDateTime((SimpleUtil.formateDateOnly(SimpleUtil.addDays(new Date(), 1))+" 00:00:00")));
				query.setFirstResult(input.getStartPosition());
				query.setMaxResults(input.getMaxPosition());
				List<Game> listGames = query.list();
				for(Game game : listGames){
					Boolean isMygame = false;
					GameDto gamesDto = new GameDto();
					if(game.getPrice() != null)
						gamesDto.setCijenaUtakmice(game.getPrice().toString());
					gamesDto.setDatumIgranja(SimpleUtil.formateDateOnly(game.getStartTime()));
					gamesDto.setIdUtakmice(game.getIdGames().toString());
					gamesDto.setLokacijaUtakmice(game.getLocation().toString());
					gamesDto.setTipUtakmice(game.getGameType().toString());
					gamesDto.setVrijemeIgranja(SimpleUtil.formateTimeOnlyWithoutSec(game.getStartTime()));
					if(game.getSifra() != null)
						gamesDto.setSifra("true");
					if(game.getPrice() != null)
						gamesDto.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game, session).toString());
					//
					//Dodavanje igrača po utakmici
					Query queryPlayers = session.createQuery("select ug from UserGame ug"
							+ " inner join fetch ug.user u "
							+ " inner join fetch ug.game g "
							+ " where g.idGames = :gameId "
							+ " order by ug.idUserGame ");
					queryPlayers.setParameter("gameId", game.getIdGames());
					query.setFirstResult(input.getStartPosition());
					query.setMaxResults(input.getMaxPosition());
					List<UserGame> playersList = queryPlayers.list();
					for(UserGame u : playersList){
						GamePlayerDto playerDto = new GamePlayerDto();
						playerDto.setUserId(u.getUser().getIdUser().toString());
						playerDto.setIme(u.getUser().getIme());
						playerDto.setPrezime(u.getUser().getPrezime());
						playerDto.setUloga(u.getUserUloga());
						if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
							playerDto.setExistAvatar("true");
						if(userId.equals(u.getUser().getIdUser())){
							isMygame = true;
						}
						gamesDto.addIgrac(playerDto);
					}		
					if(!isMygame)
						gamesDto.setHideRemoveBtn("nogometInvisible");
					result.addGameToList(gamesDto);
				}
			}else if(input.getActiveFilter().equals("sveUtakmice")){
				Query query = session.createQuery("select g from Game g "
						+ " order by g.startTime desc ");
				query.setFirstResult(input.getStartPosition());
				query.setMaxResults(input.getMaxPosition());
				List<Game> listGames = query.list();
				for(Game game : listGames){
					Boolean isMygame = false;
					GameDto gamesDto = new GameDto();
					if(game.getPrice() != null)
						gamesDto.setCijenaUtakmice(game.getPrice().toString());
					gamesDto.setDatumIgranja(SimpleUtil.formateDateOnly(game.getStartTime()));
					gamesDto.setIdUtakmice(game.getIdGames().toString());
					gamesDto.setLokacijaUtakmice(game.getLocation().toString());
					gamesDto.setTipUtakmice(game.getGameType().toString());
					gamesDto.setVrijemeIgranja(SimpleUtil.formateTimeOnlyWithoutSec(game.getStartTime()));
					if(game.getSifra() != null)
						gamesDto.setSifra("true");
					if(game.getPrice() != null)
						gamesDto.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game, session).toString());
					//
					//Dodavanje igrača po utakmici
					Query queryPlayers = session.createQuery("select ug from UserGame ug"
							+ " inner join fetch ug.user u "
							+ " inner join fetch ug.game g "
							+ " where g.idGames = :gameId "
							+ " order by ug.idUserGame ");
					queryPlayers.setParameter("gameId", game.getIdGames());					
					List<UserGame> playersList = queryPlayers.list();
					for(UserGame u : playersList){
						GamePlayerDto playerDto = new GamePlayerDto();
						playerDto.setUserId(u.getUser().getIdUser().toString());
						playerDto.setIme(u.getUser().getIme());
						playerDto.setPrezime(u.getUser().getPrezime());
						playerDto.setUloga(u.getUserUloga());
						if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
							playerDto.setExistAvatar("true");
						if(userId.equals(u.getUser().getIdUser())){
							isMygame = true;
						}
						gamesDto.addIgrac(playerDto);
					}				
					if(!isMygame)
						gamesDto.setHideRemoveBtn("nogometInvisible");
					result.addGameToList(gamesDto);
				}
			}
			result.setReturnMessage("Uspješno kreirana utakmica.");
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod dohvata utakmica.");
		}
		return result.toJson();
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/getGame")
	public String getGame(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		Naplata naplata = new Naplata();
		GetGamesInputDto input = gson.fromJson(data, GetGamesInputDto.class);
		GetGamesReturnDto result = new GetGamesReturnDto();
		Transaction tr = null;
		Session session = null;
		try {			
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();		
			Query query = session.createQuery("select g from Game g "
					+ " where g.idGames = :gameId ");
			query.setParameter("gameId", new Long(input.getGenericValue()));	
			List<Game> listGames = query.list();
			for(Game game : listGames){
				Boolean isMygame = false;
				GameDto gamesDto = new GameDto();
				if(game.getPrice() != null)
					gamesDto.setCijenaUtakmice(game.getPrice().toString());
				gamesDto.setDatumIgranja(SimpleUtil.formateDateOnly(game.getStartTime()));
				gamesDto.setIdUtakmice(game.getIdGames().toString());
				gamesDto.setLokacijaUtakmice(game.getLocation().toString());
				gamesDto.setTipUtakmice(game.getGameType().toString());
				gamesDto.setVrijemeIgranja(SimpleUtil.formateTimeOnlyWithoutSec(game.getStartTime()));
				if(game.getSifra() != null)
					gamesDto.setSifra("true");
				if(game.getPrice() != null)
					gamesDto.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game, session).toString());
				//
				//Dodavanje igrača po utakmici
				Query queryPlayers = session.createQuery("select ug from UserGame ug"
						+ " inner join fetch ug.user u "
						+ " inner join fetch ug.game g "
						+ " where g.idGames = :gameId "
						+ " order by ug.idUserGame ");
				queryPlayers.setParameter("gameId", game.getIdGames());					
				List<UserGame> playersList = queryPlayers.list();
				for(UserGame u : playersList){
					GamePlayerDto playerDto = new GamePlayerDto();
					playerDto.setUserId(u.getUser().getIdUser().toString());
					playerDto.setIme(u.getUser().getIme());
					playerDto.setPrezime(u.getUser().getPrezime());
					playerDto.setUloga(u.getUserUloga());
					if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
						playerDto.setExistAvatar("true");
					if(userId.equals(u.getUser().getIdUser())){
						isMygame = true;
					}
					gamesDto.addIgrac(playerDto);
				}				
				if(!isMygame)
					gamesDto.setHideRemoveBtn("nogometInvisible");
				result.addGameToList(gamesDto);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod dohvata utakmica.");
		}
		return result.toJson();
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/addUserToGame")
	public String addUserToGame(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		InputAddUserToGameDto input = gson.fromJson(data, InputAddUserToGameDto.class);
		ReturnAddUserToGameDto result = new ReturnAddUserToGameDto();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();			
			//Provjerimo postoji li već takav user po utakmici
			Query query = session.createQuery("select ug from UserGame ug"
					+ " inner join fetch ug.user u "
					+ " inner join fetch ug.game g "
					+ " where g.idGames = :gameId "
					+ " and u.idUser = :userId ");
			query.setParameter("gameId", new Long(input.getSelectedGameId()));
			query.setParameter("userId", new Long(input.getLogedUserId()));
			if(query.list().size() == 0){
				User user = (User) session.get(User.class, new Long(input.getLogedUserId()));
				Game game = (Game) session.get(Game.class, new Long(input.getSelectedGameId()));
				if((game.getSifra() != null && game.getSifra().equals(input.getSifra())) || game.getSifra() == null){
					if(user != null && game != null){
						//Provjerimo je li utakmica već započela i traje 10 min
						Date now = new Date();
						if(SimpleUtil.addMinutes(game.getStartTime(), 10).after(now)){
							//Kreiranje prometa za igrača kojeg trenutno dodajemo u utakmicu
							Naplata naplata = new Naplata();
							Promet promet = new Promet();
							if(game.getPrice() != null){						
								promet.setBlagajnik(game.getBlagajnik());
								promet.setUser(user);
								Double pricePerPlayer = naplata.calculatePricePerPlayer(game, session);
								Double negative = - pricePerPlayer;
								promet.setIznos(negative);		
								session.persist(promet);
							}					
							UserGame userGame = new UserGame();
							//Postavljamo ulogu korisnika
							if(game.getBlagajnik() != null){
								if(user.getIdUser() == game.getBlagajnik().getIdUser()){
									userGame.setUserUloga(UserGame.ULOGA_USER_BLAGAJNIK);
								}else{
									userGame.setUserUloga(UserGame.ULOGA_USER_IGRAC);
								}
								userGame.setPromet(promet);
							}else{
								userGame.setUserUloga(UserGame.ULOGA_USER_IGRAC);
								userGame.setPromet(null);
							}
							userGame.setGame(game);
							userGame.setUser(user);
							session.persist(userGame);
							//Korigiranje dugova po utakmici za sve prijavljene igarče
							if(game.getPrice() != null)
								naplata.updateValueOfPrometForGame(game, session);
							//Dohvaćanje svih igrača utakmice
							Query queryPlayers = session.createQuery("select ug from UserGame ug"
									+ " inner join fetch ug.user u "
									+ " inner join fetch ug.game g "
									+ " where g.idGames = :gameId "
									+ " order by ug.idUserGame ");
							queryPlayers.setParameter("gameId", game.getIdGames());
							List<UserGame> playersList = queryPlayers.list();
							Boolean isMyGame = false;
							for(UserGame u : playersList){
								//
								//Ako korisnik ima označeno da želi obavijest na mail potrebno mu je poslati mail
								if(u.getUser().getSendMail() != null && u.getUser().getSendMail().equals("yes")){
									if(EmailValidator.getInstance().isValid(u.getUser().getUssername())){
										String message = "U utakmicu ("+u.getGame().getGameType()+") koja se igra na lokaciji <b>" + u.getGame().getLocation() + "</b> u terminu " + SimpleUtil.formateDatum(u.getGame().getStartTime()) + ""
												+ " pridružio se " + user.getIme() + " " + user.getPrezime();
										message += "<br><br> Igrači utakmice: ";
										message += "<ol>";
										for(UserGame ug : playersList){
											message += "<li>"+ug.getUser().getIme()+" "+ug.getUser().getPrezime()+"</li>";
										}
										message += "</ol>";
										MailUtil mailUtil = new MailUtil("admin@tkoigra.com", u.getUser().getUssername(), "Tkoigra.com info", message);
										mailUtil.start();
									}
								}
								GamePlayerDto playerDto = new GamePlayerDto();
								playerDto.setUserId(u.getUser().getIdUser().toString());
								playerDto.setIme(u.getUser().getIme());
								playerDto.setPrezime(u.getUser().getPrezime());
								playerDto.setUloga(u.getUserUloga());
								if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
									playerDto.setExistAvatar("true");
								if(userId.equals(u.getUser().getIdUser())){
									isMyGame = true;
								}
								result.addPlayerToGame(playerDto);
							}
							if(!isMyGame)
								result.setHideRemoveBtn("nogometInvisible");
							//Dohvat podataka o utakmici kako  bi se ažurirali podaci o cijeni po igraču
							GameDto gameData = new GameDto();
							if(game.getPrice() != null){
								gameData.setCijenaUtakmice(game.getPrice().toString());
								gameData.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game, session).toString());
							}
							gameData.setDatumIgranja(SimpleUtil.formateDateOnly(game.getStartTime()));
							gameData.setLokacijaUtakmice(game.getLocation());
							gameData.setTipUtakmice(game.getGameType());
							gameData.setVrijemeIgranja(SimpleUtil.formateTimeOnly(game.getStartTime()));
							result.setGameData(gameData);
							result.setAddedUserId(user.getIdUser().toString());
							result.setReturnMessage("Uspješno ste se pridružili utakmici.");
						}else{
							result.setErrorMessage("Ne možete se pridružiti utakmici. Utakmica već traje najmanje 10 minuta!");
						}
					}
				}else{
					result.setErrorMessage("Pogrešna pristupna lozinka.");
				}
			}else{
				result.setErrorMessage("Već ste pridruženi ovoj utakmici!");
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod pridruživanja utakmici.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/deletePlayer")
	public String deletePlayer(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		InputDeletePlayerDto input = gson.fromJson(data, InputDeletePlayerDto.class);
		ReturnRemoveUserFromGameDto result = new ReturnRemoveUserFromGameDto();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			//Dohvat utakmice
			Game game = (Game)session.get(Game.class, new Long(input.getSelectedGameId()));
			Date now = new Date();
			if(game.getStartTime().after(now)){
				Query query = session.createQuery("select ug from UserGame ug"
						+ " inner join fetch ug.user u "
						+ " inner join fetch ug.game g "
						+ " where g.idGames = :gameId "
						+ " and u.idUser = :userId ");
				query.setParameter("gameId", new Long(input.getSelectedGameId()));
				query.setParameter("userId", new Long(input.getLogedUserId()));
				UserGame userGame = (UserGame)query.uniqueResult();
				//Brisanje prometa igrača
				Naplata naplata = new Naplata();
				naplata.deletePrometiOfPlayer(userGame.getGame(), userGame.getUser(), session);
				//Brisanje igrača iz utakmice
				session.delete(userGame);
				//Korigiranje dugova po utakmici za sve prijavljene igarče
				if(game.getPrice() != null)
					naplata.updateValueOfPrometForGame(game, session);
				//Dohvaćanje svih igrača utakmice
				Query queryPlayers = session.createQuery("select ug from UserGame ug"
						+ " inner join fetch ug.user u "
						+ " inner join fetch ug.game g "
						+ " where g.idGames = :gameId "
						+ " order by ug.idUserGame ");
				queryPlayers.setParameter("gameId", new Long(input.getSelectedGameId()));
				@SuppressWarnings("unchecked")
				List<UserGame> playersList = queryPlayers.list();
				Boolean isMyGame = false;
				for(UserGame u : playersList){
					//
					//Ako korisnik ima označeno da želi obavijest na mail potrebno mu je poslati mail
					if(u.getUser().getSendMail() != null && u.getUser().getSendMail().equals("yes")){
						if(EmailValidator.getInstance().isValid(u.getUser().getUssername())){
							String message = "Iz utakmice ("+u.getGame().getGameType()+") koja se igra na lokaciji <b>" + u.getGame().getLocation() + "</b> u terminu " + SimpleUtil.formateDatum(u.getGame().getStartTime()) + ""
									+ " odjavio se " + userGame.getUser().getIme() + " " + userGame.getUser().getPrezime();
							message += "<br><br> Igrači utakmice: ";
							message += "<ol>";
							for(UserGame ug : playersList){
								message += "<li>"+ug.getUser().getIme()+" "+ug.getUser().getPrezime()+"</li>";
							}
							message += "</ol>";
							MailUtil mailUtil = new MailUtil("admin@tkoigra.com", u.getUser().getUssername(), "Tkoigra.com info", message);
							mailUtil.start();
						}
					}
					GamePlayerDto playerDto = new GamePlayerDto();
					playerDto.setUserId(u.getUser().getIdUser().toString());
					playerDto.setIme(u.getUser().getIme());
					playerDto.setPrezime(u.getUser().getPrezime());
					playerDto.setUloga(u.getUserUloga());
					if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + u.getUser().getIdUser() + "_profileImg.jpg"))
						playerDto.setExistAvatar("true");
					if(userId.equals(u.getUser().getIdUser()))
						isMyGame = true;
					result.addPlayerToGame(playerDto);
				}
				if(!isMyGame)
					result.setHideRemoveBtn("nogometInvisible");
				//Dohvat podataka o utakmici kako  bi se ažurirali podaci o cijeni po igraču
				GameDto gameData = new GameDto();
				if(game.getPrice() != null){
					gameData.setCijenaUtakmice(game.getPrice().toString());
					gameData.setCijenaUtakmicePoIgracu(naplata.calculatePricePerPlayer(game, session).toString());
				}
				gameData.setDatumIgranja(SimpleUtil.formateDateOnly(game.getStartTime()));
				gameData.setLokacijaUtakmice(game.getLocation());
				gameData.setTipUtakmice(game.getGameType());
				gameData.setVrijemeIgranja(SimpleUtil.formateTimeOnly(game.getStartTime()));
				result.setGameData(gameData);
				result.setReturnMessage("Uspješno izbrisan igrač iz utakmice.");
			}else{
				result.setErrorMessage("Utakmica je započela ili je odigrana, nije više moguće izbaciti igrača iz utakmice.");
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod brisanja igrača.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/getDuznici")
	public String getDuznici(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		//Gson gson = new Gson();
		//InsertGame input = gson.fromJson(data, InsertGame.class);
		DuzniciDto result = new DuzniciDto();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Naplata naplata = new Naplata();
			result.setListaDuznika(naplata.getDuznici(userId, session));
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod dohvata dužnika.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/getKomeSamDuzan")
	public String getKomeSamDuzan(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		//Gson gson = new Gson();
		//InsertGame input = gson.fromJson(data, InsertGame.class);
		DuzniciDto result = new DuzniciDto();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createSQLQuery("select sum(pr.iznos) iznos, u.iduser idIgraca, u.ime imeIgrac, u.prezime prezimeIgrac, ub.iduser idBlagjnika, ub.ime imeBlagajnik, ub.prezime prezimeBlagajnik from prometi pr "
					+" inner join user u on pr.user = u.iduser "
					+" inner join user ub on pr.blagajnik = ub.iduser "
					+" where u.iduser = :idUser "
					+ " group by u.iduser, ub.iduser ");
			query.setParameter("idUser", new Long(userId));
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<Map> listaPrometa = query.list();			
			for(@SuppressWarnings("rawtypes") Map promet : listaPrometa){
				DuznikDto duznik = new DuznikDto();
				duznik.setIme(promet.get("imeBlagajnik").toString());
				duznik.setPrezime(promet.get("prezimeBlagajnik").toString());
				duznik.setIznos(promet.get("iznos").toString());
				result.addDuznik(duznik);
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod dohvata kome sam dužan.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/setUplata")
	public String setUplata(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Gson gson = new Gson();
		UplataDto input = gson.fromJson(data, UplataDto.class);
		DuzniciDto result = new DuzniciDto();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			User uplatitelj = (User) session.get(User.class, new Long(input.getIdKorisnika()));
			User blagajnik = (User) session.get(User.class, new Long(userId));
			Double iznosUplate = new Double(input.getIznosUplate());
			Promet uplata = new Promet();
			uplata.setBlagajnik(blagajnik);
			uplata.setIznos(iznosUplate);
			uplata.setUser(uplatitelj);
			session.persist(uplata);
			//Dohvat novog stanja dužnika
			Naplata naplata = new Naplata();			
			result.setListaDuznika(naplata.getDuznici(userId, session));
			result.setReturnMessage("Uspješno unesena uplata.");
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod upisa uplate.");
		}
		return result.toJson();
	}
}
