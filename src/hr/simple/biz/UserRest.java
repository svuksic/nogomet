package hr.simple.biz;

import hr.simple.dto.GoogleUserDto;
import hr.simple.dto.MergeUserDto;
import hr.simple.dto.MergeUserReturnDto;
import hr.simple.dto.RegistracijaInputDto;
import hr.simple.dto.UserDto;
import hr.simple.entity.GrupaKorisnika;
import hr.simple.entity.Identity;
import hr.simple.entity.User;
import hr.simple.hibernate.HibernateUtil;
import hr.simple.util.MailUtil;
import hr.simple.util.RestResultSet;
import hr.simple.util.SimpleUtil;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gson.Gson;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/loginHandler")
public class UserRest {
	
	@Context ServletContext context;

	@POST
	@Path("/login")
	public String login(String data){
		String result = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tr = session.beginTransaction();
		result = loginUtil(data, session, tr);
		tr.commit();
		session.close();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public String loginUtil(String data, Session session, Transaction tr){
		final Integer PERIOD = 10;
		String result = null;
		User logedUser = null;
		Gson gson = new Gson();
		UserDto user = gson.fromJson(data, UserDto.class);		
		//
		//Prvo potrebno provjeriti postoji li korisnik s danim korisničkim imenom
		Query query = session.createQuery("select u from User u " +
				" inner join fetch u.grupa gr " +
				" where u.ussername = :user");
		query.setParameter("user", user.getUsername());
		List<User> userList = query.list();
		if(userList.size() > 0){
			logedUser = userList.get(0);
			//
			//Ako postoji treba provjeriti slaže li se i lozinka s tim korisničkim imenom
			query = session.createQuery("select u from User u " +
					" inner join fetch u.grupa gr " +
					" where u.ussername = :user and u.password = :pass ");
			query.setParameter("user", user.getUsername());
			query.setParameter("pass", user.getPassword());
			userList = query.list();
			if(userList.size() > 0 && userList.get(0).getNumberWrongLogin() < 3){				
				String idString = logedUser.getIme()+logedUser.getPrezime()+new Date().getTime();
				MessageDigest md;
				byte[] bytesOfMessage;
				try {
					md = MessageDigest.getInstance("MD5");
					bytesOfMessage = idString.getBytes("UTF-8");
					byte[] thedigest = md.digest(bytesOfMessage);
					user = new UserDto();
					user.setIme(logedUser.getIme());
					user.setPrezime(logedUser.getPrezime());
					user.setToken(thedigest.toString());
					user.setGrupa(logedUser.getGrupa().getIdGk());
					user.setSifra(logedUser.getIdUser());
					logedUser.setToken(thedigest.toString());
					logedUser.setLastWrongLogin(null);
					logedUser.setNumberWrongLogin(0);
					session.merge(logedUser);				
					result =  gson.toJson(user);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedEncodingException ex) {
					ex.printStackTrace();
				}
			}else{
				Date now = new Date();
				if(logedUser.getLastWrongLogin() != null && logedUser.getNumberWrongLogin() == 2){
					long diff = now.getTime() - logedUser.getLastWrongLogin().getTime();
					long diffMinutes = diff / 1000 / 60;
					if(diffMinutes > PERIOD){
						logedUser.setLastWrongLogin(null);
						logedUser.setNumberWrongLogin(0);
						session.merge(logedUser);
						session.flush();
					}
				}
				//
				//Ako za dano korisničko ime ne postoji lozinka dati korisniku još 2 pokušaja upisa lozinke				
				if(logedUser.getNumberWrongLogin() < 2){
					Integer numberOfTry = logedUser.getNumberWrongLogin() + 1;
					logedUser.setLastWrongLogin(new Date());
					logedUser.setNumberWrongLogin(numberOfTry);
					session.merge(logedUser);
					session.flush();
					RestResultSet resultDto = new RestResultSet();
					Integer restNuberOfTry = new Integer(3)-numberOfTry;
					resultDto.setReturnMessage("Pogrešni korisnički podaci, preostalo još "+restNuberOfTry+" pokušaja.");
					result = gson.toJson(resultDto);
				}else if(logedUser.getNumberWrongLogin() == 2){
					Integer numberOfTry = logedUser.getNumberWrongLogin() + 1;
					logedUser.setLastWrongLogin(new Date());
					logedUser.setNumberWrongLogin(numberOfTry);
					session.merge(logedUser);
					session.flush();					
					RestResultSet resultDto = new RestResultSet();
					long diff = now.getTime() - logedUser.getLastWrongLogin().getTime();
					long diffMinutes = diff / 1000 / 60;
					Long restMinutes = 10 - diffMinutes;
					resultDto.setReturnMessage("Zbog prevelikog broja neispravni pokušaja sljedećih "+restMinutes+" minuta nemate pravo prijave u sustav.");
					result = gson.toJson(resultDto);
				}else{					
					//Ako je prošlo manje od 10 min.
					now = new Date();
					long diff = now.getTime() - logedUser.getLastWrongLogin().getTime();
					long diffMinutes = diff / 1000 / 60;
					if(diffMinutes < PERIOD){
						Long restMinutes = 10 - diffMinutes;
						RestResultSet resultDto = new RestResultSet();
						resultDto.setReturnMessage("Zbog prevelikog broja neispravni pokušaja sljedećih "+restMinutes+" minuta nemate pravo prijave u sustav.");
						result = gson.toJson(resultDto);
					}else{
						logedUser.setLastWrongLogin(null);
						logedUser.setNumberWrongLogin(0);
						session.merge(logedUser);
						session.flush();						
						result = loginUtil(data, session, tr);
					}
				}
			}
		}else if(userList.size() == 0){
			RestResultSet resultDto = new RestResultSet();
			resultDto.setReturnMessage("Nema korisnika");
			result = gson.toJson(resultDto);
		}							
		return result;
	}
	

	@POST
	@Path("/signIn")
	public String signIn(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		RestResultSet result = new RestResultSet();
		Transaction tr = null;
		Session session = null;
		Gson gson = new Gson();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			
			RegistracijaInputDto insertObject = gson.fromJson(data, RegistracijaInputDto.class);			
			//
			//Provjera postoji li već takvi username i password
			Query query = session.createQuery("select u from User u where u.ussername = :user and u.password = :pass ");
			query.setParameter("user", insertObject.getUsername());
			query.setParameter("pass", insertObject.getPassword());
			//Ako već takav korisnik potoji u sustavu vraćamo poruku o grešci
			if(query.list().size() > 0){
				result.setErrorMessage("Korisnik sa jednakim korisničkim imenom i lozinkom već postoji u sustavu. ");
			}else{					
				GrupaKorisnika grupaEntity = new GrupaKorisnika();
				grupaEntity.setNazivGk(insertObject.getIme()+"_"+insertObject.getPrezime()+"_grupa");
				session.save(grupaEntity);
				
				User userEntity = new User();
				userEntity.setGrupa(grupaEntity);
				userEntity.setIme(insertObject.getIme());
				userEntity.setPrezime(insertObject.getPrezime());
				userEntity.setUssername(insertObject.getUsername());
				userEntity.setPassword(insertObject.getPassword());
				userEntity.setNumberWrongLogin(0);
				userEntity.setActiveUser("yes");
				session.save(userEntity);
				
				result.setReturnMessage("Registracija uspješno izvršena.");
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod registracije u sustav.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/insertUser")
	public String insertUserCrudUser(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data){
		String result = "";
		Gson gson = new Gson();
		UserDto user = gson.fromJson(data, UserDto.class);
		Session session = HibernateUtil.getSessionFactory().openSession();
		try{
			Transaction tr = session.beginTransaction();
			User userEntity = new User();
			userEntity.setIme(user.getIme());
			userEntity.setPrezime(user.getPrezime());
			userEntity.setUssername(user.getUsername());
			userEntity.setPassword(user.getPassword());	
			userEntity.setGrupa((GrupaKorisnika)session.get(GrupaKorisnika.class, grupa));
			userEntity.setOib(user.getOib());
			userEntity.setNumberWrongLogin(0);
			session.save(userEntity);
			tr.commit();
			result = userEntity.getIdUser().toString();
		}catch(HibernateException e){
			result = "Greška kod unosa korisnika.";
		}	
		session.close();
		return result;
	}
	
	@POST
	@Path("/getUsers")
	public String getAllUsers(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String firstRow){
		String result = null;
		Transaction tr = null;
		Session session = null;
		Gson gson = new Gson();
		Integer firstRowInt = null;
		if(firstRow == null && !StringUtils.equals(firstRow, "")){
			firstRowInt = new Integer(0);
		}else{
			firstRowInt = new Integer(firstRow);
		}
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("select u from User u inner join fetch u.grupa g where g.idGk = :grupa ");
			query.setParameter("grupa", grupa);
			query.setFirstResult(firstRowInt);
			query.setMaxResults(10);
			@SuppressWarnings("unchecked")
			List<User> lista = query.list();
			result = gson.toJson(lista);
			tr.commit();
			session.close();
		}catch(Exception ex){
			tr.rollback();
			session.close();
			result = "Greška kod dohvata svih korisnika.";
		}
		
		return result;
	}
	
	@POST
	@Path("/searchUsers")
	public String searchUsers(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data){
		String result = null;
		Transaction tr = null;
		Session session = null;
		Gson gson = new Gson();
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("select u from User u inner join fetch u.grupa g where (u.ime like :ime and u.activeUser = 'yes' or u.ime like :ime and u.activeUser is null)  or (u.prezime like :prezime and u.activeUser = 'yes' or u.prezime like :prezime and u.activeUser is null) ");
			//query.setParameter("grupa", grupa);
			query.setParameter("ime", "%"+data+"%");
			query.setParameter("prezime", "%"+data+"%");
			if(SimpleUtil.isNumeric(data)){
				query = session.createQuery("select u from User u inner join fetch u.grupa g where u.idUser = :id and u.activeUser = 'yes' or u.idUser = :id and u.activeUser is null");
				
				query.setParameter("id", new Long(data));
			}			
			@SuppressWarnings("unchecked")
			List<User> lista = query.list();
			result = gson.toJson(lista);
			tr.commit();
			session.close();
		}catch(Exception ex){
			tr.rollback();
			session.close();
			result = "Greška kod pretraživanja korisnika.";
		}		
		return result;
	}
	
	@POST
	@Path("/getUserById")
	public String getUserById(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data){
		String result = null;
		Transaction tr = null;
		Session session = null;
		Gson gson = new Gson();
		try{
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("select u from User u inner join fetch u.grupa g where g.idGk = :grupa and u.idUser = :id ");
			query.setParameter("id", new Long(data));
			query.setParameter("grupa", grupa);
			@SuppressWarnings("unchecked")
			List<User> lista = query.list();
			User user = lista.get(0);
			UserDto userDto = new UserDto();
			userDto.setIme(user.getIme());
			userDto.setPrezime(user.getPrezime());
			userDto.setUsername(user.getUssername());
			userDto.setPassword(user.getPassword());
			userDto.setIdUser(user.getIdUser().toString());
			userDto.setSendMail(user.getSendMail());
			if(SimpleUtil.existFile(context.getRealPath("/img") + File.separator + user.getIdUser() + "_profileImg.jpg"))
				userDto.setExistAvatar("true");
			query = session.createQuery("select i from Identity i "
					+ " inner join fetch i.user u "
					+ " inner join fetch u.grupa gr "
					+ " where u.idUser = :userId ");
			query.setParameter("userId", userId);
			@SuppressWarnings("unchecked")
			List<Identity> identityList = query.list();			
			if(identityList.size() > 0){
				Identity identity = identityList.get(0);
				userDto.setIsMerged(identity.getMerged());
			}
			result = gson.toJson(userDto);
			tr.commit();
			session.close();
		}catch(Exception ex){
			tr.rollback();
			session.close();
			ex.printStackTrace();
			result = "Greška kod dohvata korisnika za editiranje.";
		}
		
		return result;
	}
	
	@POST
	@Path("/editUser")
	public String editUser(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data){
		String result = null;
		Transaction tr = null;
		Session session = null;
		Gson gson = new Gson();
		try{			
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			User userEntity = gson.fromJson(data, User.class);
			User saveUser = (User)session.get(User.class, userEntity.getIdUser());
			saveUser.setIme(userEntity.getIme());
			saveUser.setPrezime(userEntity.getPrezime());
			saveUser.setUssername(userEntity.getUssername());
			saveUser.setPassword(userEntity.getPassword());
			saveUser.setSendMail(userEntity.getSendMail());
			User azuriranUser = (User)session.merge(saveUser);
			result = azuriranUser.getIdUser().toString();
			tr.commit();
			session.close();
		}catch(Exception ex){
			tr.rollback();
			session.close();
			result = "Greška kod editiranja korisnika.";
		}
		
		return result;
	}
	
	@POST
	@Path("/deleteUser")
	public String deleteUser(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data){		
		Transaction tr = null;
		Session session = null;
		String result = "";
		try{			
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			User user = new User();
			user.setIdUser(new Long(data));
			session.delete(user);
			tr.commit();
			session.close();
			result = "Korisnik uspješno obrisan.";
		}catch(Exception ex){
			tr.rollback();
			session.close();
			result = "Greška kod brisanja korisnika.";
		}
		return result;
	}
	
	@POST
	@Path("/logout")
	public String logout(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		RestResultSet result = new RestResultSet();
		Transaction tr = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			User user = (User) session.get(User.class, userId);
			//
			//Brisanje tokena
			user.setToken("");
			session.merge(user);
			result.setReturnMessage("Uspješna odjava.");
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod odjave korisnika.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/uploadProfilePicture")
	@Consumes(MediaType.MULTIPART_FORM_DATA)	
	public String uploadProfImg(@FormDataParam("file") InputStream stream, @FormDataParam("file") FormDataContentDisposition contentDispositionHeader, @HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId) {		
		RestResultSet resultSet = new RestResultSet();
		try{	
			String fileName = userId + "_profileImg.jpg";
			String imgFolder = context.getRealPath("/img");
			//Kreiranje foldera u koji ce s spremiti logo
			String filePath = imgFolder + File.separator + fileName;
			//Spremanje loga
			SimpleUtil.saveFile(filePath, stream);
			resultSet.setReturnMessage("Slika profila je uspješno prenesena.");
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Greška kod prijenosa slike profila.");
			resultSet.setErrorMessage("Greška kod prijenosa slike profila..");
		}		
		return resultSet.toJson();
	}
	
	@POST
	@Path("/deleteAvatar")
	public String deleteAvatar(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		RestResultSet result = new RestResultSet();
		try{	
			String fileName = userId + "_profileImg.jpg";
			String imgFolder = context.getRealPath("/img");
			File avatar = new File(imgFolder + File.separator + fileName);
			//Brisanje svog sadrzaja foldera
			if(avatar.exists()){
				avatar.delete();
			}
			result.setReturnMessage("Slika profila je uspješno izbrisana.");
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Greška kod brisanja slike profila.");
			result.setErrorMessage("Greška kod brisanja slike profila.");
		}	
		return result.toJson();
	}
	
	@POST
	@Path("/googleSignIn")
	public String googleSignIn(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		UserDto result = new UserDto();
		Transaction tr = null;
		Session session = null;
		try{	
			Gson gson = new Gson();
			GoogleUserDto googleUser = gson.fromJson(data, GoogleUserDto.class);

			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("select i from Identity i "
					+ " inner join fetch i.user u "
					+ " inner join fetch u.grupa gr "
					+ " where i.identityUserId = :userId ");
			query.setParameter("userId", googleUser.getId());
			@SuppressWarnings("unchecked")
			List<Identity> identityList = query.list();			
			if(identityList.size() == 0){				
				//Dodavnje novog usera
				User user = new User();				
				String ime = googleUser.getDisplayName().split(" ")[0];
				String prezime = googleUser.getDisplayName().split(" ")[1];
				user.setIme(ime);
				user.setPrezime(prezime + " - Google korisnik");
				GrupaKorisnika grupaEntity = new GrupaKorisnika();
				grupaEntity.setNazivGk(ime+"_"+prezime+"_grupa");
				session.save(grupaEntity);
				user.setGrupa(grupaEntity);
				user.setNumberWrongLogin(0);
				user.setLastWrongLogin(null);
				user.setActiveUser("yes");
				session.persist(user);
				
				Identity newIden = new Identity();
				newIden.setIdentityUserId(googleUser.getId());
				newIden.setProvider("Google");
				newIden.setUser(user);
				newIden.setMerged("no");
				session.persist(newIden);
				
				String idString = user.getIme()+user.getPrezime()+new Date().getTime();
				MessageDigest md;
				byte[] bytesOfMessage;
				md = MessageDigest.getInstance("MD5");
				bytesOfMessage = idString.getBytes("UTF-8");
				byte[] thedigest = md.digest(bytesOfMessage);
				result.setIme(user.getIme());
				result.setPrezime(user.getPrezime());
				result.setToken(thedigest.toString());
				result.setGrupa(user.getGrupa().getIdGk());
				result.setSifra(user.getIdUser());
				user.setToken(thedigest.toString());
				session.persist(user);
			}else{
				Identity identity = identityList.get(0);
				User logedUser = identity.getUser();
				String idString = logedUser.getIme()+logedUser.getPrezime()+new Date().getTime();
				MessageDigest md;
				byte[] bytesOfMessage;
				md = MessageDigest.getInstance("MD5");
				bytesOfMessage = idString.getBytes("UTF-8");
				byte[] thedigest = md.digest(bytesOfMessage);
				result.setIme(logedUser.getIme());
				result.setPrezime(logedUser.getPrezime());
				result.setToken(thedigest.toString());
				result.setGrupa(logedUser.getGrupa().getIdGk());
				result.setSifra(logedUser.getIdUser());
				logedUser.setToken(thedigest.toString());
				logedUser.setLastWrongLogin(null);
				logedUser.setNumberWrongLogin(0);
				session.merge(logedUser);				
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod prijave korisnika putem Google računa.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/mergeUser")
	public String mergeUser(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		MergeUserReturnDto result = new MergeUserReturnDto();
		Transaction tr = null;
		Session session = null;
		try{	
			Gson gson = new Gson();
			MergeUserDto mergeUser = gson.fromJson(data, MergeUserDto.class);

			session = HibernateUtil.getSessionFactory().openSession();
			tr = session.beginTransaction();
			Query query = session.createQuery("select u from User u "
					+ " inner join fetch u.grupa gr "
					+ " where u.ussername = :ussername "
					+ " and u.password = :password ");
			query.setParameter("ussername", mergeUser.getUssername());
			query.setParameter("password", mergeUser.getPassword());
			@SuppressWarnings("unchecked")
			List<User> userList = query.list();			
			if(userList.size() > 0){
				//Dohvaćenog usera ćemo prodružiti trenutnom identitiu
				query = session.createQuery("select i from Identity i "
						+ " inner join fetch i.user u "
						+ " inner join fetch u.grupa gr "
						+ " where u.idUser = :userId ");
				query.setParameter("userId", userId);
				@SuppressWarnings("unchecked")
				List<Identity> identityList = query.list();			
				if(identityList.size() > 0){
					//Dohvatili smo identity trenbutno rpijavljenog Google usera, njemu treba pridružiti usera iz tkoigra.com za kojeg su ipisani podaci
					Identity identity = identityList.get(0);
					//Korisnika koje je kreiran s Google prijavom ćemo proglasiti neaktivnim
					//kako se ne bi pojavljivao u lookupu za blagajnika
					User googleUser = (User)session.get(User.class, identity.getUser().getIdUser());
					googleUser.setActiveUser("no");
					session.merge(googleUser);
					//
					User user = userList.get(0);
					identity.setUser(user);
					identity.setMerged("yes");
					session.merge(identity);
					result.setMergeUserId(user.getIdUser().toString());
					result.setMergeGroupId(user.getGrupa().getIdGk().toString());
					result.setReturnMessage("Uspješno ste spojili račune!");
				}
			}else{
				result.setReturnMessage("Ne postoji takav korisnik prijavljen na tkoigra.com!");
			}
			tr.commit();
			session.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			tr.rollback();
			session.close();
			result.setErrorMessage("Greška kod pridruživanja korisnika.");
		}
		return result.toJson();
	}
	
	@POST
	@Path("/sendMail")
	public String sendMail(@HeaderParam("grupa") Long grupa, @HeaderParam("user") Long userId, String data) {
		Transaction tr = null;
		Session session = null;
		RestResultSet result = new RestResultSet();
		String mail = data.replaceAll("\"", "");
		if(EmailValidator.getInstance().isValid(mail)){
			try{				
				session = HibernateUtil.getSessionFactory().openSession();
				tr = session.beginTransaction();
				Query query = session.createQuery("select u from User u "
						+ " inner join fetch u.grupa gr "
						+ " where u.ussername = :ussername ");
				query.setParameter("ussername", mail);
				@SuppressWarnings("unchecked")
				List<User> userList = query.list();
				if(userList.size() > 0){
					User user = userList.get(0);
					String msgBody = "<br>Vaša lozinka za tkoigra.com je: <b>" + user.getPassword() + "</b>";
					msgBody += "<br><br> Pozdrav od tkoigra.com!";
					MailUtil mailUtil = new MailUtil("admin@tkoigra.com", user.getUssername(), "Tkoigra.com info", msgBody);
					mailUtil.start();
					result.setReturnMessage("Mail je uspješno poslan");
				}else{
					result.setErrorMessage("Ne postoji korisnik s takvom e-mail adresom!");
				}
				tr.commit();
				session.close();
			}catch(Exception ex){
				result.setErrorMessage("Greška kod slanja maila!");
				ex.printStackTrace();
				tr.rollback();
				session.close();
			}
		}else{
			result.setErrorMessage("Neispravan format e-mail adrese!");
		}		
		return result.toJson();
	}
}
