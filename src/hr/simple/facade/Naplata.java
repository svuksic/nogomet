package hr.simple.facade;

import hr.simple.dto.DuznikDto;
import hr.simple.entity.Game;
import hr.simple.entity.Promet;
import hr.simple.entity.User;
import hr.simple.entity.UserGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * Sve metode vezane za izračunavanje cijena utakmice, ažuriranje prometa itd...
 * @author Svuksic
 *
 */
public class Naplata {
	
	/**
	 * Izračunava kolika je cijena utakmice po igraču
	 * @param game
	 * @param session
	 * @return
	 */
	public Double calculatePricePerPlayer(Game game, Session session){
		Double playerDebt = null;
		Integer numberOfPlayers = numberOfPlayers(game, session);
		if(numberOfPlayers > 0 && game.getPrice() != null){
			playerDebt = game.getPrice() / numberOfPlayers;
		}else{
			playerDebt = game.getPrice();
		}
		return playerDebt;
	}
	
	/**
	 * Vraća broj igrača za određenu utakmicu
	 * @param game
	 * @param session
	 * @return
	 */
	public Integer numberOfPlayers(Game game, Session session){
		Integer playerNumbers = null;
		Query query = session.createQuery("select ug from UserGame ug "
				+ " inner join fetch ug.game g "
				+ " where g.idGames =:gameId");
		query.setParameter("gameId", game.getIdGames());
		playerNumbers = query.list().size();
		return playerNumbers;
	}
	
	/**
	 * Ažurira iznose prometa po utakmici za sve igrače kako bi ukupno stanje bilo ok
	 * @param game
	 * @param session
	 */
	public void updateValueOfPrometForGame(Game game, Session session){
		Double pricePerPlayer = calculatePricePerPlayer(game, session);
		Query query = session.createQuery("select ug from UserGame ug "
				+ " inner join fetch ug.game g "
				+ " inner join fetch ug.promet p "
				+ " where g.idGames =:gameId");
		query.setParameter("gameId", game.getIdGames());
		@SuppressWarnings("unchecked")
		List<UserGame> list = query.list();
		for(UserGame userGame : list){
			Promet promet = userGame.getPromet();
			promet.setIznos(-pricePerPlayer);
			session.merge(promet);
		}
	}
	
	/**
	 * Briše sve promete za igrača koji je maknut iz utakmice
	 * @param game
	 * @param user
	 * @param session
	 */
	public void deletePrometiOfPlayer(Game game, User user, Session session){
		Query query = session.createQuery("select ug from UserGame ug "
				+ " inner join fetch ug.game g "
				+ " inner join fetch ug.promet p "
				+ " inner join fetch ug.user u "
				+ " where g.idGames =:gameId "
				+ " and u.idUser = :userId ");
		query.setParameter("gameId", game.getIdGames());
		query.setParameter("userId", user.getIdUser());
		@SuppressWarnings("unchecked")
		List<UserGame> list = query.list();
		for(UserGame userGame : list){
			Promet promet = userGame.getPromet();
			session.delete(promet);
		}
	}
	
	/**
	 * Vraća dužnike trenutno prijavljenog korisnika
	 * @param userId
	 * @param session
	 * @return
	 */
	public List<DuznikDto> getDuznici(Long userId, Session session){
		List<DuznikDto> listaDuznika = new ArrayList<DuznikDto>();
		Query query = session.createSQLQuery("select sum(pr.iznos) iznos, u.iduser idIgraca, u.ime imeIgrac, u.prezime prezimeIgrac, ub.iduser idBlagjnika, ub.ime imeBlagajnik, ub.prezime prezimeBlagajnik from prometi pr "
				+" inner join user u on pr.user = u.iduser "
				+" inner join user ub on pr.blagajnik = ub.iduser "
				+" where ub.iduser = :idUser "
				+ " group by u.iduser, ub.iduser ");
		query.setParameter("idUser", userId);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<Map> listaPrometa = query.list();			
		for(@SuppressWarnings("rawtypes") Map promet : listaPrometa){
			DuznikDto duznik = new DuznikDto();
			duznik.setIme(promet.get("imeIgrac").toString());
			duznik.setPrezime(promet.get("prezimeIgrac").toString());
			duznik.setIznos(promet.get("iznos").toString());
			duznik.setIdKorisnika(promet.get("idIgraca").toString());
			listaDuznika.add(duznik);
		}
		return listaDuznika;
	}
	
	public List<User> getGameUsers(Game game, Session session){
		List<User> userList = new ArrayList<User>();
		Query query = session.createQuery("select ug from UserGame ug "
				+ " inner join fetch ug.game g "
				+ " inner join fetch ug.user u "
				+ " where g.idGames =:gameId" );
		query.setParameter("gameId", game.getIdGames());
		@SuppressWarnings("unchecked")
		List<UserGame> list = query.list();
		for(UserGame userGame : list){
			User user = userGame.getUser();
			userList.add(user);
		}
		return userList;
	}
}
