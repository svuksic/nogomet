package hr.simple.pageHandler;

import java.io.IOException;
import java.nio.charset.Charset;

import hr.simple.util.SimpleUtil;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;


/**
 * Web servis koji sluši za dohvacanje svih ekrana 
 * Argument je naziv ekrana kojeg šelimo dohvatiti
 * @author Svuksic
 *
 */

@Path("/pageResolver")
public class PageHandlerRest {
	@javax.ws.rs.core.Context 
	ServletContext context;
	
	@GET
	@Path("/{param}")
	public String getPage(@PathParam("param") String pageIdent) {

		String output = null;
		String page = null;
		String errorPage = null;
		
		String path = context.getRealPath("/WEB-INF/ekrani/" + pageIdent);
		try {
			page = SimpleUtil.readFile(path, Charset.forName("UTF8"));
			output = page;
			System.out.println("Putanja do ekrana: /WEB-INF/ekrani/" + pageIdent);
		} catch (IOException e) {
			e.printStackTrace();
			try {
				errorPage = SimpleUtil.readFile(path+"empty.html", Charset.forName("UTF8"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}						
			output = errorPage;
		}
		return output;
	}
}
