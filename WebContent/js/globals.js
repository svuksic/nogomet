//trenutno su ovdje spremljeni globalni podaci, s vremenom je potrebno naci pametnije rje�enje
//od sada se ovi podaci spremaju u store od preglednika, tu služe samo kao popis za lakše programiranje
var ussername;
var password;
var ime;
var prezime;
var securityToken;
var grupa;
var userId;
var sessionIdBlagajne;

//Id utakmice koja je dobivena putem url-a
var urlGameId;

//samo za kreiranje novg na temelju storno racuna
var iznosStorna;

//pokazatelj da postoji greška kod validacije forme, ako je postavljeno na true save gumb se ne smije izvršiti
var indikatorGreskeValidacije = false;

//trenutni ekrana koji se prikazuje
var currentScreen;

//odziv na onekeyup globalna varijabla 350 ms
var odziv = 300;

//trajanje prikaza poruke
var trajanjePoruke = 5000;

function test(){
	alert("Tu sam, funkcija iz globala!");
}

/**
 * from http://www.naprej.com/format_currency_euro_in_javascript_
 * ti currency every string
 * @param iznos
 * @returns
 */



/**
 * add 'h' to end of input value for time
 * @param inputId
 */
function toTimeFormat(inputId){
	value = $("#"+inputId).val();
	$("#"+inputId).val(value+"h");
}

/**
 * //provjerava je li unos u input numericki
 */
function validateNumeralInput(inputId){
	value = $("#"+inputId).val();
	if(value.indexOf("kn")){
		value = value.replace("kn","");
	}
	if(value.indexOf("%")){
		value = value.replace("%","");
	}
	if($.isNumeric(value) || value == ""){
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		indikatorGreskeValidacije = false;
	}else{
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>Numeričko polje.</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("has-warning");
		$("#"+inputId).val(value.substring(0,value.length - 1));
		indikatorGreskeValidacije = true;
	}
}


/**
 * remove end 'h' char from input value
 * @param inputId
 * @returns
 */
function removeTimePosfixInput(inputId){
	return $("#"+inputId).val().replace("h","");
}
/**
 * remove end 'h' char from string
 * @param value
 * @returns
 */
function removeTimePosfixValue(value){
	return value.replace("h","");
}

/**
 * //provjerava je li unos u input numericki i ako je provjerava duzinu upisa
 */
function validateNumeralInputAndSize(elem, maxSize){
	value = $(elem).val();
	if(value.indexOf("kn")){
		value = value.replace("kn","");
	}
	if(value.indexOf("%")){
		value = value.replace("%","");
	}
	if($.isNumeric(value) || value == ""){
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		validateStringSize(inputId, maxSize);
		indikatorGreskeValidacije = false;
	}else{
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>Numeričko polje.</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("has-warning");
		$("#"+inputId).val(value.substring(0,value.length - 1));
		indikatorGreskeValidacije = true;
	}
}

/**
 * provjerava duljinu stringa
 */
function validateStringSize(inputId, maxSize){
	value = $("#"+inputId).val();
	if(value.indexOf("%") != -1 || value.indexOf("kn") != -1){
		maxSize = maxSize + 1;
	}
	if(value.length <= maxSize){
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		indikatorGreskeValidacije = false;
	}else{
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>Duljina unosa veća od dopuštene.</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("has-warning");
		$("#"+inputId).val(value);
		indikatorGreskeValidacije = true;
	}
}

/**
 * provjerava duljinu stringa, jedan argument je poruka 
 * every input has to have parent div for validation message
 * @param inputId
 * @param maxSize
 * @param poruka
 */
function validateStringSizePoruka(inputId, maxSize, poruka){
	value = $("#"+inputId).val();
	if(value.length <= maxSize){
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		indikatorGreskeValidacije = false;
	}else{
		divControls = $("#"+inputId).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>"+poruka+"</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("has-warning");
		$("#"+inputId).val(value);
		indikatorGreskeValidacije = true;
	}
}




//za skirivanje i otkrivanje elemenata
function togleElement(idItem){
	$("#"+idItem).toggle();
}

function showAjaxSpiner(){
	$("#ajaxLoad").show();
}

function hideAjaxSpiner(){
	$("#ajaxLoad").hide();
}

/**
 * fix table header for CRUD forms
 * @param idTable
 * @param headerHeight
 * @param filter
 */
function fixTableHeader(idTable, filter){
	
	var filterHeight = $("#"+filter).height();
	
	var listaOfWidthTh = [];
	//sprema vrijednosti prije fiksiranja da ih mogu postaviti nakon
	$("#"+idTable+" thead tr:first th").each(function(i, val){
		listaOfWidthTh[i] = $(val).outerWidth();
	});
	
	//fiksira header
	$("#"+idTable+" thead tr:first").css("position","fixed");
	//ovo -1 je tu zbog razmaka koji se pojavljuje na tabletima između tablice i filtera
	filterHeight = filterHeight - 1;
	$("#"+idTable+" thead tr:first").css("margin-top",filterHeight+"px");
	$("#"+idTable+" thead tr:first").css("z-index",1005);
	
	
	//iteracija po rvom redu kako bi uzeli sirinu celija prvog reda
	//i tu sirinu preslikali na fiksni header
	$("#"+idTable+" thead tr:first th").each(function(i, val){
		$(val).outerWidth(listaOfWidthTh[i]);
		$(val).css("background-color","white");				
	});
	//postavlja sirinu celija tbody-a
	$("#"+idTable+" tbody tr:first td").each(function(i, val){
		$(val).outerWidth(listaOfWidthTh[i]);
	});
	
	var headerHeight = $("#"+idTable+" thead tr:first").height();
	var sumHeight = headerHeight + filterHeight;
	$("#spacer").parent().remove();
	$("#spacer").remove();
	$("#"+idTable+" thead:first").append("<tr><th id='spacer'><th></tr>");
	$("#spacer").css("height",sumHeight+"px");
	$("#spacer").css("padding","0px");
	$("#spacer").css("margin","0px");
}

/**
 * 
 * @param id
 * @param toggleAction - call function on toggle 
 * @param untoggleAction - call function on untoggle
 */
function toggleButtonGlobal(id, toggleAction, untoggleAction){	
	if($("#"+id+"").attr("data-toggle") == undefined || $("#"+id+"").attr("data-toggle") == "false"){		
		$("#"+id+"").button('toggle');
		$("#"+id+"").attr("data-toggle","true");
		if(toggleAction != null && toggleAction != undefined && toggleAction != "null" && toggleAction != ""){
			toggleAction();
		}
	}else{
		untoggleButtonGlobal(id);	
		if(untoggleAction != null && untoggleAction != undefined && untoggleAction != "null" && untoggleAction != ""){
			untoggleAction();
		}
	}
}

function untoggleButtonGlobal(id){
	$("#"+id+"").attr("data-toggle","false");
	$("#"+id+"").removeClass("active");	
}

/**
 * prikazivanje i sakrivanje generičkih poruka
 * @param message
 */
function showGenericOkMessage(message){
	$("#genericOkMessage").html("");
	$("#genericOkMessage").html(message+'<button type="button" onclick="HideGenericOkMessage();" class="close">&times;</button>');
	$("#genericOkMessage").show();
	setTimeout(function() {HideGenericOkMessage();}, trajanjePoruke);
}
/**
 * 
 * @param message
 */
function showGenericErrorMessage(message){
	$("#genericErrorMessage").html("");
	$("#genericErrorMessage").html(message+'<button type="button" onclick="HideGenericErrorMessage();" class="close">&times;</button>');
	$("#genericErrorMessage").show();
	setTimeout(function() {HideGenericErrorMessage();}, trajanjePoruke);
}

function HideGenericErrorMessage(){
	$("#genericErrorMessage").hide();
}

function HideGenericOkMessage(){
	$("#genericOkMessage").hide();
}

/**
 * prazni tablicu definiranu id-om
 */
function removeFromTable(idTable){
	$('#'+idTable+' > tbody').empty();
}

/**
 * Generic function for getting screen over ajax
 * @param screen - screen you want to display
 * @param preFunction - function before ajax
 * @param postFunction - function after ajax
 * @param params - parametri za callback funkciju
 * @param popup - ako je true znači da dohvaćamo ekran za popup i ne postavlja se globalna varijabla current screen
 */
function getScreen(screen, preFunction, postFunction, params, popup){
	$.ajax({
		  type: "GET",
		  url: "rest/pageResolver/"+screen,
		  beforeSend: function (request){  			  
			  request.setRequestHeader("Authority", sessionStorage.getItem("securityToken"));
              request.setRequestHeader("Grupa", sessionStorage.getItem("grupa"));
              showAjaxSpiner();
              if(preFunction != null)
            	  preFunction();
        }
		}).done(function(page) {
			if(!popup)
				currentScreen = screen;
			hideAjaxSpiner();
			if(postFunction != null){
				if(params != null && params != undefined){
					postFunction(page, params);
				}else{					
					postFunction(page);
				}
				
			}
				
		});
}


/**
 * open new screen in new popup window
 * @param data
 * @param title title of popup screen
 * @returns
 */
function popup(data, title) 
{
	params  = 'width=auto';
	params += ', height=auto';
	params += ', top=0, left=0';
	params += ', fullscreen=yes';
	params += ', toolbar=no';
	params += ', location=no';
	params += ', menubar=no';
    var mywindow = window.open('', 'Ispis_racuna', params);    
    mywindow.document.write('<html><head><title>'+title+'</title>');
    mywindow.document.write('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">');
    mywindow.document.write('<link href="css/custom.css" rel="stylesheet" media="screen">');
    mywindow.document.write('<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');    
    if (window.focus) {mywindow.focus();}  
    return mywindow;
}

/**
 * ajax call
 * @param restUrl rest url
 * @param data data to transfer
 * @param preFunction function before ajax
 * @param postFunction function after ajax
 */
function getAjaxCall(restUrl, data, preFunction, postFunction, postParams){
	var stringData = "";
	if(data != null && data != undefined && data != "null" && data != ""){
		if(typeof data === 'string'){
			stringData = data.toString();
		}else{
			stringData = JSON.stringify(data);
		}			
	}else{
		stringData = "".toString();
	}
	
	$.ajax({
		  type: "POST",
		  url: "rest/"+restUrl,
		  data: stringData,
		  beforeSend: function (request){
              request.setRequestHeader("Authority", sessionStorage.getItem("securityToken"));
              request.setRequestHeader("Grupa", sessionStorage.getItem("grupa"));
              request.setRequestHeader("User", sessionStorage.getItem("userId"));
              if(preFunction!=null){
            	  preFunction();
              }	  
              showAjaxSpiner();
        },
		  error : function(xhr, ajaxOptions, thrownError) {
			  hideAjaxSpiner();
			  $("#alertMessage").show();
			  console.log("ajax error: "+thrownError);
			  return false;
		  },
		  success:function(returnedData) {	
			  hideAjaxSpiner();
			  if(returnedData != "authErr"){
				  if(returnedData != "" && returnedData != undefined){
					  returnedData = JSON.parse(returnedData);
					  if(returnedData.errorMessage != undefined){
						  console.log("ajax error "+returnedData.errorMessage);
						  showGenericErrorMessage(returnedData.errorMessage);
					  }else{
						  if(postFunction != undefined && postFunction != null && postFunction != "null"){
							  if(postParams != null && postParams != undefined){
								  postFunction(returnedData, postParams); 
							  }	else{
								  postFunction(returnedData);
							  }							  
						  }								  						  
					  }
				  }
			}else{
				hideAjaxSpiner();
				console.log("ajax autentification error.");
				showGenericErrorMessage("Greška kod autentificiranja korisnika. Potrebno se ponovno prijaviti.");						
			}
		  }
		});
}

/**
 * ajax call without user autentification
 * @param restUrl rest url
 * @param data data to transfer
 * @param preFunction function before ajax
 * @param postFunction function after ajax
 */
function getAjaxCallAnonimus(restUrl, data, preFunction, postFunction, postParams){
	if(data != null && data != undefined && data != "null" && data != ""){
		stringData = JSON.stringify(data);
	}else{
		stringData = "";
	}
	
	$.ajax({
		  type: "POST",
		  url: "rest/"+restUrl,
		  data: stringData,
		  beforeSend: function (request){
              if(preFunction!=null){
            	  preFunction();
              }	  
              showAjaxSpiner();
        },
		  error : function(xhr, ajaxOptions, thrownError) {
			  hideAjaxSpiner();
			  $("#alertMessage").show();
			  console.log("ajax error: "+thrownError);
			  return false;
		  },
		  success:function(returnedData) {	
			  hideAjaxSpiner();
			  if(returnedData != "authErr"){
				  if(returnedData != "" && returnedData != undefined){
					  returnedData = JSON.parse(returnedData);
					  if(returnedData.errorMessage != undefined){
						  console.log("ajax error "+returnedData.errorMessage);
						  showGenericErrorMessage(returnedData.errorMessage);
					  }else{
						  if(postFunction != undefined && postFunction != null && postFunction != "null"){
							  if(postParams != null && postParams != undefined){
								  postFunction(returnedData, postParams); 
							  }	else{
								  postFunction(returnedData);
							  }							  
						  }								  						  
					  }
				  }
			}else{
				hideAjaxSpiner();
				console.log("ajax autentification error.");
				showGenericErrorMessage("Greška kod autentificiranja korisnika. Potrebno se ponovno prijaviti.");						
			}
		  }
		});
}

/**
 * parse form and create object 
 * @param divId content div of form
 */
function getFormObject(divId) {
	formObject = null;
	if (divId != null && divId != undefined) {
		formObject = new Object();
		var elms = document.getElementById(divId).getElementsByTagName("*");
		for ( var i = 0; i < elms.length; i++) {
			//only for input elements
			if (elms[i].tagName.toLowerCase() === "input" && $(elms[i]).attr("type") === "text" || $(elms[i]).attr("type") === "password") {
				var input = elms[i];
				//depends on metadata - type of data
				if($(input).attr("data-valueType") != null && $(input).attr("data-valueType") != undefined
						&& $(input).attr("data-valueType") == "timeSufiks"){
					formObject[input.id] = removeTimePosfixValue(input.value);
				}else if($(input).attr("data-valueType") != null && $(input).attr("data-valueType") != undefined
						&& $(input).attr("data-valueType") == "currency"){
					formObject[input.id] = transformCurrencyValue(input.value);
				}else if($(input).attr("data-valueType") != null && $(input).attr("data-valueType") != undefined
						&& $(input).attr("data-valueType") == "percentage"){
					formObject[input.id] = transformPercentageValue(input.value);
				}else{
					formObject[input.id] = input.value;
				}				
			}else if(elms[i].tagName.toLowerCase() === "select"){
				var select = elms[i];
				formObject[select.id] = select.value;
			}else if(elms[i].tagName.toLowerCase() === "textarea"){
				var textArea = elms[i];
				formObject[textArea.id] = $(textArea).val();
			}
			//TODO rest elements
		}
	}
	return formObject;
}

/**
 * reset all form elements
 * @param divId
 */
function clearForm(divId) {
	if (divId != null && divId != undefined) {
		formObject = new Object();
		var elms = document.getElementById(divId).getElementsByTagName("*");
		for ( var i = 0; i < elms.length; i++) {
			//only for input elements
			if (elms[i].tagName.toLowerCase() === "input") {
				var input = elms[i];
				input.value = "";			
			}
			//TODO rest elements
		}
	}
}

/**
 * fill form with properties from data object
 * @param dataObject
 * @param actionAfter call it after fill form
 */
function fillForm(dataObject){
	for(var propertyName in dataObject) {
		if(typeof dataObject[propertyName] === "object" && propertyName != "gson" && !dataObject[propertyName].hasOwnProperty('selectId')){
			fillForm(dataObject[propertyName]);
		}else{	
			var elem = document.getElementById(propertyName);
			if(elem != undefined && elem != null){
				//only for input elements
				if (elem.tagName.toLowerCase() === "input"){
					elem.value = dataObject[propertyName];
				}else if(elem.tagName.toLowerCase() === "textarea"){
					$(elem).val(dataObject[propertyName]);
				}else if(elem.tagName.toLowerCase() === "select"){
					selectValueByKey(elem.id, dataObject[propertyName]);
				}
				//TODO rest elements
			}
		}
	}
}

/**
 * file upload over ajax
 * @param url
 * @param file
 * @param callbackFunction
 */
function getAjaxUpload(restUrl, file, preFunction, postFunction){
	var formData = new FormData();
	formData.append("file",file);
	
	$.ajax({
	    url: "rest/"+restUrl,
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: "POST",
	    beforeSend: function (request){
            request.setRequestHeader("Authority", sessionStorage.getItem("securityToken"));
            request.setRequestHeader("Grupa", sessionStorage.getItem("grupa"));
            request.setRequestHeader("User", sessionStorage.getItem("userId"));
            if(preFunction!=null){
          	  preFunction();
            }	  
            showAjaxSpiner();
        },
        error : function(xhr, ajaxOptions, thrownError) {
			  hideAjaxSpiner();
			  $("#alertMessage").show();
			  console.log("ajax error: "+thrownError);
			  return false;
		},
	    success: function(returnedData){
	    	hideAjaxSpiner();
			  if(returnedData != "authErr"){
				  if(returnedData != "" && returnedData != undefined){
					  returnedData = JSON.parse(returnedData);
					  if(returnedData.errorMessage != undefined){
						  console.log("ajax error "+returnedData.errorMessage);
						  showGenericErrorMessage(returnedData.errorMessage);
					  }else{
						  if(postFunction != undefined && postFunction != null && postFunction != "null"){
							  postFunction(returnedData); 
						  }								  						  
					  }
				  }
			  }
	    }
	});
	
}


/**
 * if session storage is implemented return true
 * @returns
 */
function isSuportedSessionStorage(){
	result = true;
	if (Modernizr.sessionStorage) {
		result = true;
	} else {
		result = false;
	}
	return result;
}

/**
 * format date string to local date format
 * @param inputDate
 * @returns {String}
 */
function formatDate(inputDate){
	if(inputDate != null && inputDate != undefined){
		var date = new Date(inputDate);
		var outputDate = date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear()+".";
		return outputDate;
	}else{
		return "";
	}
}

/**
 * format date-time string to local date format
 * @param inputDate
 * @returns {String}
 */
function formatDateTime(inputDate){
	var date = new Date(inputDate);
	var outputDate = date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear()+". "+date.getHours()+":"+date.getMinutes();
	return outputDate;
}

/**
 * Za loadanje na scroll
 * @param inputDataSet . objekt kojem se obavezno definiraju atributi startPosition i maxPosition, a na
 * serverskoj strani data argument mora biti bilo koji objekt koji nasljeđuje RestInputSet ili on sam 
 * jer ons sadrži gore navedene atribute.
 * @param dataFunction - funkcija koja loada podatke, najčešće neki ajax koji puni tablicu
 * @param screenName - naziv ekrana na kojem se poziva ova funkcija, npr. crudPoslovniProstor.html - u nekoj od budućih
 * iteracija ovaj argument će se možda izbaciti, aut omatski će se dohvaćati naziv ekrana
 * @window - $(window) - referenca na window
 */
function scrollKit(inputDataSet, dataFunction, screenName){
	//ako je od prethodnog ekrana ostao scroll da se ponisti
	//čuda neka, možda postoji i pametnije rješenje ali ja trenutno ne znam
	$(window).unbind();
	window.setTimeout(function(){	
		$(window).scroll(function() {
			if(currentScreen == screenName){
				var visina = $(document).height() - $(window).height();
				//alert("Scroltop:"+$(window).scrollTop()+"document height"+visina);
			    if($(window).scrollTop() >= ($(document).height() - $(window).height() -2 ) && $(window).scrollTop() != 0) {
			    	inputDataSet.startPosition = inputDataSet.startPosition + inputDataSet.maxPosition;
			    	dataFunction(inputDataSet);
			    }
			}
		});
	},500);
}

/**
 * fill select control
 * @param selectId - id of select tag
 * @param listOfObjects - lista objekata koje želimo prikazati unutar select kontrole
 * @param key - naziv propertia koji će biti ključ
 * @param value - naziv propertia koji će biti vrijesnost
 * @param initValue - prikazuje li se inicijalna stavka ili ne
 */
function fillSelect(selectId,listOfObjects, key, value, initValue){
	$("#"+selectId).html("");
	if(initValue)
		$("#"+selectId).append('<option selected value="init">Odaberite...</option>');
	var listOfValueProperties = value.split(".");
	var listOfKeyProperties = key.split(".");
	for(var i=0; i < listOfObjects.length; i++){
		var itemForValue = listOfObjects[i];
		var itemForKey = listOfObjects[i];
		//dio za value
		
		var val = "";
		if(listOfValueProperties.length == 1){
			prop = listOfValueProperties[0];
			val = itemForValue[prop];
		}else if(listOfValueProperties.length > 1){
			for(var ii=0; ii < listOfValueProperties.length; ii++){				
				prop = listOfValueProperties[ii];
				if(typeof itemForValue[prop] === "object"){
					itemForValue = itemForValue[prop];
				}else{
					val = itemForValue[prop];
				}				
			}
		}
		//dio za key		
		var key = "";
		if(listOfKeyProperties.length == 1){
			prop = listOfKeyProperties[0];
			key = itemForKey[prop];
		}else if(listOfKeyProperties.length > 1){
			for(var ii=0; ii < listOfKeyProperties.length; ii++){				
				prop = listOfKeyProperties[ii];
				if(typeof itemForKey[prop] === "object"){
					itemForKey = itemForKey[prop];
				}else{
					key = itemForKey[prop];
				}				
			}
		}		
		$("#"+selectId).append('<option value="'+key+'">'+val+'</option>');
	}
}

/**
 * odabere vrijednost u select kontroli prema ključu
 * @param selectId - id kontrole 
 * @param key - ključ koji želimo odabrati
 */
function selectValueByKey(selectId,key){
	$("#"+selectId+" option").each(function(i,item){
		if($(item).attr("value") == key){
			$(item).attr("selected",true);
		}
	}); 
}

//***********************************************************************************
///***********************VALIDACIJE*************************************************
//***********************************************************************************

/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//>>NOVČANI IZNOSI - VALIDACIJE, PRETVPRBE
/**
 * transform currency format to number from input field
 * @param inputId
 */
function transformCurrencyInput(inputId){
	if($("#"+inputId).val().indexOf("kn") > -1){
		return $("#"+inputId).val().replace("kn","").replace(".","").replace(",",".");
	}else if($("#"+inputId).val().indexOf("Kn") == -1){
		return $("#"+inputId).val();
	}
}
/**
 *  transform currency format to number
 * @param value
 * @returns
 */
function transformCurrencyValue(value){
	if(value.indexOf("kn") > -1){
		return value.replace("kn","").replace(".","").replace(",",".");
	}else if(value.indexOf("Kn") == -1){
		return value;
	}	
}

/**
 * validira cijenu u inputu
 * 100% testirana i radi
 */
function validateCurencyInput(elem){
	result = "";
	value = $(elem).val();
	if(value.match('^[0-9]+\.[0-9]{1,2}$')){
		result = "ok";
	}else if(value.match('^[0-9]+\,[0-9]{1,2}$')){
		result = "ok";
	}else if(value.match('^[0-9]*$')){
		result = "ok";
	}else if(value.match('^[0-9]+\,[0-9]{1,2}kn$')){
		result = "ok";
	}else if(value.match('^[0-9]+(\.[0-9]+)+?\,[0-9]{1,2}kn$')){
		result = "ok";
	}else if(value.match('^[0-9]+\,[0-9]{1,2}Kn$')){
		result = "ok";
	}else if(value.match('^[0-9]+(\.[0-9]+)+?\,[0-9]{1,2}Kn$')){
		result = "ok";
	}else{
		result = "error";
	}
	if(result === "error"){
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>Neispravan format cijene.</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("warning");
	}else if(result === "ok"){
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		toCurrencyInput(elem);
	}	
}

/**
 * 100% testirana i radi
 * from http://www.naprej.com/format_currency_euro_in_javascript_
 * ti currency every string
 * @param iznos
 * @returns
 */
function toCurrency(number)
{
	if(number != undefined){
		var numberStr = parseFloat(number).toFixed(2).toString();
		var numFormatDec = numberStr.slice(-2); /*decimal 00*/
		numberStr = numberStr.substring(0, numberStr.length-3); /*cut last 3 strings*/
		var numFormat = new Array;
		while (numberStr.length > 3) {
			numFormat.unshift(numberStr.slice(-3));
			numberStr = numberStr.substring(0, numberStr.length-3);
		}
		numFormat.unshift(numberStr);
		return numFormat.join('.')+','+numFormatDec; /*format 000.000.000,00 */
	}
}

/**
 * format input value to currency format 
 * @param inputId
 */
function toCurrencyInput(elem){	
	value = $(elem).val();
	if(value != null && value != undefined){
		value = value.replace(",",".");
		value = value.replace("kn","");
		if($.isNumeric(value)){
			$(elem).val(toCurrency(value)+"kn");		
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//<<NOVČANI IZNOSI - VALIDACIJE, PRETVPRBE

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//<<RAD S POSTOCIMA
/**
 * validira je li ispravno unešen postotak
 * 100% testirana i radi
 */
function validatePercentageInput(elem){
	result = "";
	value = $(elem).val();
	if(value.match('^[0-9]{1,2}(\.[0-9]{1,2})?$')){
		result = "ok";
	}else if(value.match('^[0-9]{1,2}(\,[0-9]{1,2})?$')){
		result = "ok";
	}else if(value.match('^[0-9]{1,2}\,[0-9]{1,2}%$')){
		result = "ok";
	}else if(value.match('^[0-9]{1,2}\.[0-9]{1,2}%$')){
		result = "ok";
	}else if(value.match('^[0-9]{1,2}%?$')){
		result = "ok";
	}else{
		result = "error";
	}
	if(result === "error"){
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControls.append("<span id='validatePoruka' class='help-inline'>Neispravan format postotka.</span>");		
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		divControulGroup.addClass("warning");
	}else if(result === "ok"){
		divControls = $(elem).parent();
		divControls.find("span").remove();
		divControulGroup = divControls.parent();
		divControulGroup.removeClass("has-warning");
		setPostotakInput(elem);
	}	
}

/**
 * set '%' to the end of input value
 * @param inputId
 */
function setPostotakInput(elem){	
	value = $(elem).val();
	if(value != null && value != undefined){
		value = value.replace("%","");
		value = value.replace(".",",");
		$(elem).val(value+"%");
	}
}

/**
 * transform percentage format to number from input field
 * @param inputId
 */
function transformPercentageInput(inputId){
	return $("#"+inputId).val().replace("%","").replace(",",".");	
}
/**
 * transform percentage format to number
 * @param value
 * @returns
 */
function transformPercentageValue(value){
	return value.replace("%","").replace(",",".");	
}
/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//>>RAD S POSTOCIMA

/**
 * Glavna funkcija koja provjerava je li bilo validacijskig grešaka prije submita forme
 * Poziva i funkciju koja provjerava jesu li popunjena obavezna polja
 * Ako postoji greška u validaciji forme funkcija vraca true
 */
function isValidationError(formId){	
	result = false;
	form = $("#"+formId);
	if($(form.find("[class~=has-warning]")).length > 0 && $(form.find("[class~=has-warning]")).length > 0){
		result = true;
	}else{
		if(validateIsThereEmptyField(formId) == true){
			result = true;
		}
	}
	return result;
}

/**
 * prolazi kroz cijelu formu i na svako obavezno polje stavlja poruku
 * obavezno na polje koje se ispituje staviti atribut data-required
 * Ako ima greške vraća false
 * divId - id forme za koju ispitujemo
 */
function validateIsThereEmptyField(divId){
	result = false;
	var greska = false;
	if (divId != null && divId != undefined) {
		formObject = new Object();
		var elms = document.getElementById(divId).getElementsByTagName("*");		
		for ( var i = 0; i < elms.length; i++) {
			if (elms[i].tagName.toLowerCase() === "input") {
				var input = $(elms[i]);
				if(input.attr("data-required") != undefined && input.val() == ""){
					divControls = input.parent();
					divControls.find("span").remove();
					input.after("<span id='validatePoruka"+input.attr("id")+"' class='help-inline' style='color:#8a6d3b;'>Obavezno polje</span>");		
					divControulGroup = divControls.parent();
					divControulGroup.removeClass("has-warning");
					divControulGroup.addClass("has-warning");
					greska = true;
					inputId = input.attr("id");
					$("#"+inputId).keyup(function(){
						if($(this).val() != ""){
							divControls = $(this).parent();
							divControls.find("span").remove();								
							divControulGroup = divControls.parent();
							divControulGroup.removeClass("has-warning");
						}
					});
				}else{
					divControls = $(elms[i]).parent();
					divControls.find("span").remove();
					divControulGroup = divControls.parent();
					divControulGroup.removeClass("has-warning");
				}
			}
		}
		if(greska){
			result = true;
		}else{
			result = false;
		}
	}
	return result;
}

function getToday(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd;
	} 

	if(mm<10) {
	    mm='0'+mm;
	} 

	var todayDate = dd+"-"+mm+"-"+yyyy;
	return todayDate; 
}


