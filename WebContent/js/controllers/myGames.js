/**
 * 
 */
var myGamesController = {
		getGamesInputDto : new Object(),
		selectedGame : null,
		init : function(){
			myGamesView.init();
			//Ako je korisnik došao na ekran putem dobivenog url-a sa id-om utakmice onda prikazujemo samo tu utakmicu			
			if(urlGameId != "" && urlGameId != undefined){
				indexView.deselectSviFilteri();
				myGamesController.getGamesInputDto.genericValue = urlGameId;
				//Resetiramo dobiveni id utakmice
				urlGameId = "";
				getAjaxCall("gameHandler/getGame", myGamesController.getGamesInputDto, null, myGamesView.addGames);				
			}else{
				//Load init data			
				myGamesController.getGamesInputDto.activeFilter = "danasnjeUtakmice";
				//Scroll loading			
				myGamesController.getGamesInputDto.startPosition = 0;
				myGamesController.getGamesInputDto.maxPosition = 6;
				myGamesController.getGames(myGamesController.getGamesInputDto);
				scrollKit(myGamesController.getGamesInputDto,myGamesController.getGames,"myGames.html");
				myGamesView.clearGamesList();
			}
		},
		//
		//
		linkMyGames_onClick : function(e){
			myGamesView.deselectAllLinksFromMenu();
			myGamesView.selectMyGamesLink();	
			getScreen("myGames.html", null, indexView.setContentToMainFrame);
			e.preventDefault();
		},
		linkMyProfile_onClick : function(){
			myGamesView.deselectAllLinksFromMenu();
			myGamesView.selectMyProfileLink();
			getScreen("myProfile.html", null, indexView.setContentToMainFrame);
		},
		logout_onClick : function(){
			getAjaxCall("loginHandler/logout",null,null,this.afterLogout);
		},
		linkCreateGame_onClick : function(){
			myGamesView.deselectAllLinksFromMenu();
			myGamesView.selectCreateGameLink();
			getScreen("createGame.html", null, indexView.setContentToMainFrame);
		},
		afterLogout : function(){
			myGamesView.hideLogedUser();
			indexView.clearMenu();
			indexView.addToMenu(indexView.menuItem1);
			indexView.addToMenu(indexView.menuItem2);
			sessionStorage.clear();
			localStorage.clear();
			getScreen("login.html", null, indexView.setContentToMainFrame);
			//Ako postoji google sesija odjaviti korisnika sa googla 
			gapi.auth.signOut();
		},
		getGames : function(getGamesInputDto){			
			getAjaxCall("gameHandler/getGames", getGamesInputDto, null, myGamesView.addGames);
		},
		//Pridružuje trenutnog korisnika utakmici
		addPlayer_onClick : function(gameId){
			var inputData = new Object();
			myGamesController.selectedGame = gameId;
			inputData.logedUserId = sessionStorage.getItem("userId");
			inputData.selectedGameId = gameId;
			if($("#game_"+gameId).attr("data-sifra") == "true"){
				$("#sifraModal").modal();
				myGamesView.sifraInput.val("");
			}
//			else if($("#game_"+gameId).attr("data-price") != "undefined"){
//				swal({
//					title: "Jeste li sigurni da se želite pridružiti utakmici?",   
//					text: "Ova utakmica se naplaćuje, ulaskom u igru obevezujete se blagajniku platiti traženi iznos. ",   
//					type: "warning",   
//					showCancelButton: true,   
//					cancelButtonText: "Ne želim",
//					confirmButtonColor: "#DD6B55",   
//					confirmButtonText: "Želim igrati!",  				
//					closeOnConfirm: true }, 
//					function(){   
//						getAjaxCall("gameHandler/addUserToGame", inputData, null, myGamesView.afterAddUserToGame, gameId); 
//					});
//			}
			else{
				getAjaxCall("gameHandler/addUserToGame", inputData, null, myGamesView.afterAddUserToGame, gameId);
			}
		},		
		removeUser_onClick : function(userId, gameId){
			swal({
				title: "Jeste li sigurni da želite izbaciti igrača iz utakmice?",   
				text: "Ova opcija je korisna u slučaju da se igrač nije samostalno odjavio iz utakmice, a nije se pojavio na njoj.",   
				type: "warning",   
				showCancelButton: true,   
				cancelButtonText: "Ne želim",
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Želim!",  				
				closeOnConfirm: true }, 
				function(){   
					var inputDto = new Object();
					inputDto.logedUserId = userId;
					inputDto.selectedGameId = gameId;
					getAjaxCall("gameHandler/deletePlayer", inputDto, null, myGamesView.afterDeleteKorisnik, gameId); 
				});
		},
		sifraBtn_onClick : function(){
			var sifra = myGamesView.sifraInput.val();
			if(sifra != undefined){
				var inputData = new Object();
				inputData.logedUserId = sessionStorage.getItem("userId");
				inputData.selectedGameId = myGamesController.selectedGame;
				inputData.sifra = sifra;
				swal({
					title: "Jeste li sigurni da se želite pridružiti utakmici?",   
					text: "Ova utakmica se naplaćuje, ulaskom u igru obevezujete se blagajniku platiti traženi iznos. ",   
					type: "warning",   
					showCancelButton: true,   
					cancelButtonText: "Ne želim",
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Želim igrati!",  				
					closeOnConfirm: true }, 
					function(){   
						getAjaxCall("gameHandler/addUserToGame", inputData, null, myGamesView.afterAddUserToGame, myGamesController.selectedGame); 
					});
			}			
		},
		getGameLink : function(gameId){			
			$("#linkToGame").html(window.location.protocol + "//" + window.location.host + window.location.pathname + "?gameId=" + gameId);
			$("#gameLinkModal").modal();
		}
};

var myGamesView = {		
		sifraBtn : $("#sifraBtn"),
		sifraInput : $("#sifra"),
		menuItem1 : "<li class='liMyGames active'><a class='linkMyGames standardApplicationLink' data-toggle='collapse' data-target='#menuList'>Utakmice</a></li>",
		menuItem2 : "<li class='liMyProfile'><a class='linkMyProfile standardApplicationLink' data-toggle='collapse' data-target='#menuList'>Moj profil</a></li>",		
		menuItem4 : "<li class='liCreateGame'><a class='linkCreateGame standardApplicationLink' data-toggle='collapse' data-target='#menuList'>Kreiraj utakmicu</a></li>",
		menuItem5 : "<li class='liOdjava'><a class='linkOdjava standardApplicationLink visible-xs hidden-lg hidden-md hidden-sm' onclick='myGamesController.logout_onClick();' data-toggle='collapse' data-target='#menuList'>Odjava</a></li>",
        gamesList : $(".gamesList"),
        //
        //
		init : function(){
			indexView.showFilterMyGames();
			indexView.deselectSviFilteri();
			indexView.selectDanasnjeUtakmice();
			indexView.clearMenu();			
			indexView.addToMenu(this.menuItem1);
			indexView.addToMenu(this.menuItem4);
			indexView.addToMenu(this.menuItem2);
			indexView.addToMenu(this.menuItem5);
			//indexView.addToMenu(this.menuItem3);			
			$(".linkMyGames").click(myGamesController.linkMyGames_onClick);
			$(".linkMyProfile").click(myGamesController.linkMyProfile_onClick);
			$(".linkCreateGame").click(myGamesController.linkCreateGame_onClick);
			myGamesView.sifraBtn.click(myGamesController.sifraBtn_onClick);			
			this.setLogedUser();			
		},
		clearGamesList : function(){
			myGamesView.gamesList.html("");
		},
        //
		//Glavna funkcija za dodavanje utakmica na ekran
		addGames: function(getGamesReturnDto){			
			var gamesListVar = getGamesReturnDto.gamesList;
			//Dodavanje utakmica
			for(var i=0; i<gamesListVar.length; i++){
				var gameDto = gamesListVar[i];
				var headertext;
				if(gameDto.cijenaUtakmice != undefined)
					headertext = gameDto.tipUtakmice + ", " + gameDto.lokacijaUtakmice + ", Cijena: " + toCurrency(gameDto.cijenaUtakmice) + "Kn, Po igraču: " + toCurrency(gameDto.cijenaUtakmicePoIgracu) + "Kn";
				else
					headertext = gameDto.tipUtakmice + ", " + gameDto.lokacijaUtakmice;
				myGamesView.gamesList.append(myGamesView.createGameTemplate(gameDto.datumIgranja, gameDto.vrijemeIgranja, headertext, gameDto.idUtakmice, gameDto.cijenaUtakmice, gameDto.sifra));
				//Dodavanje igrača				
				var playersList = gameDto.igraciUtakmice;
				var rb = 1;
				for(var ii=0; ii<playersList.length; ii++){
					var player = playersList[ii];
					myGamesView.gamesList.find("#userContainer_"+gameDto.idUtakmice).append(myGamesView.createUserTemplate(rb,player.ime,player.prezime,player.uloga,player.userId,gameDto.idUtakmice, gameDto.hideRemoveBtn, player.existAvatar));
					rb++;
				}
				
			}			
		},
		updateGames : function(getGamesReturnDto){
			
		},
		afterAddUserToGame : function(returnedData, gameId){
			showGenericOkMessage(returnedData.returnMessage);
			myGamesView.updatePlayersForGame(gameId, returnedData, returnedData.hideRemoveBtn);			
		},
		//
		//
        createUserTemplate: function(redniBroj, name, surname, uloga, userId, gameId, btnCssClass, existAvatar){
        	var imgName;
        	if(existAvatar == "true"){
        		imgName = "img/" + userId + "_profileImg.jpg";
        	}else{
        		imgName = "img/plainUser.png";
        	}
        	
        	var userName;
        	if(userId ==  sessionStorage.getItem("userId")){
        		userName = "<a class='standardApplicationLink' onclick='myGamesController.linkMyProfile_onClick();' title='Moj profil'><span class='imePrezimeUtakmica'>"+name+" "+surname+"</span></a>";
        	}else{
        		userName = "<span class='imePrezimeUtakmica'>"+name+" "+surname+"</span>";
        	}
        	
	        var gameUsers = "<div id='userMainFrame_"+userId+"' class='row userMainFrame'>"+
			"<div class='col-lg-1 col-md-1 col-xs-1'>"+
			"<span class='redniBrojIgraca'>"+redniBroj+"</span>"+
			"</div>"+
	        "<div class='col-lg-2 col-md-2 col-xs-2 userFrame'>"+
	            "<img alt='User Pic' src='"+imgName+"' class='img-circle userPicture'>"+                                    
	        "</div>"+
	        "<div class='col-lg-9 col-md-9 col-xs-8 userData'>"+
	        	"<div class='row'>" +
	        		"<div class='col-lg-4 col-md-4 col-xs-8'>"+
			        	userName +
			            "<p>"+
			                uloga+                                   
			            "</p>"+
		            "</div>"+
		            "<div class='col-lg-2 col-md-2 col-xs-4'>"+
		            	"<button id='btnRemove_"+userId+"' type='button' onclick='myGamesController.removeUser_onClick("+userId+","+gameId+")' class='btn btn-danger "+btnCssClass+"'>" +
		            	"<span class='glyphicon glyphicon-remove'/>" +
		            	" Makni</button>"+
		            "</div>"+
	        	"</div>"+            
	        "</div>"+
	        "</div>";
	        return gameUsers;
        },
        //
        //
        createGameTemplate : function(date, time, headerText, gameId, price, sifra){
        	return "<li data-price='"+price+"' data-sifra='"+sifra+"' id='game_"+gameId+"'>"+
			"<time class='cbp_tmtime'><span>"+date+"</span> <span>"+time+"</span></time>"+
			"<div class='cbp_tmicon glyphicon glyphicon-time'></div>"+
			"<div class='cbp_tmlabel form-horizontal'>"+
				"<div class='row userHeader form-group'>"+
					"<div class='col-lg-9 col-md-9 col-xs-6'>"+
						"<h3 id='userHeaderText_"+gameId+"' class='userHeaderText'>"+headerText+"</h3>"+
					"</div>"+
					"<div class='col-lg-2 col-md-2 col-xs-4'>"+
						"<button type='button' class='btn btn-primary' onclick='myGamesController.addPlayer_onClick("+gameId+")'>" +
						"<span class='glyphicon glyphicon-plus'></span> "+
						"Pridruži me " +						
						"</button>"+						
					"</div>" +
					"<div class='col-lg-1 col-md-1 col-xs-2'>" +
					"<span id='gameLink' onclick='myGamesController.getGameLink("+gameId+")' class='glyphicon glyphicon-link linkIcon' data-toggle='tooltip' data-placement='top' title='Pošalji link na utakmicu prijatelju.'></span>" +
					"</div>"+
				"</div>"+
				"<div id='userContainer_"+gameId+"'>"+
                "</div>"+                            						
			"</div>"+						
		"</li>";
        },
        updatePlayersForGame : function(gameId, returnedData, btnClass){
        	var playersList = returnedData.igraciUtakmice;
        	$("#userContainer_"+gameId).html("");
        	var rb = 1;
			for(var ii=0; ii<playersList.length; ii++){
				var player = playersList[ii];
				if(returnedData.addedUserId == player.userId){
					var playerFrame = myGamesView.createUserTemplate(rb,player.ime,player.prezime,player.uloga,player.userId,gameId,btnClass,player.existAvatar);
					$(playerFrame).hide().appendTo("#userContainer_"+gameId).fadeIn(500);
				}else{
					myGamesView.gamesList.find("#userContainer_"+gameId).append(myGamesView.createUserTemplate(rb,player.ime,player.prezime,player.uloga,player.userId,gameId,btnClass,player.existAvatar));
				}
				rb++;
			}
			var headertext;
			var gameDto = returnedData.gameData;
			if(gameDto.cijenaUtakmice != undefined)
				headertext = gameDto.tipUtakmice + ", " + gameDto.lokacijaUtakmice + ", Cijena: " + toCurrency(gameDto.cijenaUtakmice) + "Kn, Po igraču: " + toCurrency(gameDto.cijenaUtakmicePoIgracu) + "Kn";
			else
				headertext = gameDto.tipUtakmice + ", " + gameDto.lokacijaUtakmice;
			$("#userHeaderText_"+gameId).html(headertext);
        },        
        //
        //
		setLogedUser : function(){
			indexView.logedUser.html("<b>"+ sessionStorage.getItem("ime") + " " + sessionStorage.getItem("prezime") + "</b><a id='odjavaLink' onclick='myGamesController.logout_onClick();' style='text-decoration: none; cursor: pointer;'> (Odjava)</a> ");
			$(".linkOdjava").html("Odjava (" + sessionStorage.getItem("ime") + " " + sessionStorage.getItem("prezime") + ")" );
		},
		selectMyGamesLink : function(){
			$(".liMyGames").addClass("active");
		},
		selectMyProfileLink : function(){
			$(".liMyProfile").addClass("active");
		},
		selectCreateGameLink : function(){
			$(".liCreateGame").addClass("active");
		},
		deselectAllLinksFromMenu : function(){
			$(".liMyGames").removeClass("active");
			$(".liMyProfile").removeClass("active");
			$(".liBlagajna").removeClass("active");
			$(".liCreateGame").removeClass("active");
		},
		hideLogedUser : function(){
			indexView.logedUser.html("");
		},
		afterDeleteKorisnik : function(returnedData, gameId){
			showGenericOkMessage(returnedData.returnMessage);
			myGamesView.updatePlayersForGame(gameId, returnedData, returnedData.hideRemoveBtn);
		}
};

var myGamesModel = {
};