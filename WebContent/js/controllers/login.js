/**
 * Main js class for login screen
 */

var loginController = {	
		//
		//Entry point for screen object
		//Poziva se sa html templatea		
		init : function(){	
			$(document).ready(function() {
				//Poziva sve ostale init metode
				loginView.init();
				loginModel.init();
			});
		},

		btnPrijava_onclick : function(){
			getAjaxCallAnonimus("loginHandler/login",getFormObject("loginFrame"),null,loginController.afterLogin);
		},
		
		btnRegistracija_onclick : function(){
			if(!isValidationError("registrationFrame")){
				getAjaxCallAnonimus("loginHandler/signIn", getFormObject("registrationFrame"), null, loginController.afterSignIn);
			}
		},
		
		afterLogin : function(data){			
			if(data.returnMessage == undefined){
				if(!loginView.zapamtiMe.prop("checked")){
					if(isSuportedSessionStorage){						
						//setaju se globali
						sessionStorage.setItem("ime",data.ime);
						sessionStorage.setItem("prezime",data.prezime);
						sessionStorage.setItem("securityToken",data.token);
						sessionStorage.setItem("userId",data.sifra);
						sessionStorage.setItem("grupa",data.grupa);
						//loginView.hideLoginMessage();
						getScreen("myGames.html",null,indexView.setContentToMainFrame);						
					}else{
						showGenericErrorMessage("Session storage is not suported. Change web browser.");
						//loginView.loginMessage.html("Session storage is not suported. Change web browser.");
						//loginView.showLoginMessage();
					}
				}else{
					//Session storage se postavlja jer ajax funkcija koristi njih
					sessionStorage.setItem("ime",data.ime);
					sessionStorage.setItem("prezime",data.prezime);
					sessionStorage.setItem("securityToken",data.token);
					sessionStorage.setItem("userId",data.sifra);
					sessionStorage.setItem("grupa",data.grupa);
					//Local storage se postavljaju zbog sesije prijave
					localStorage.setItem("ime",data.ime);
					localStorage.setItem("prezime",data.prezime);
					localStorage.setItem("securityToken",data.token);
					localStorage.setItem("userId",data.sifra);
					localStorage.setItem("grupa",data.grupa);
					//loginView.hideLoginMessage();
					getScreen("myGames.html",null,indexView.setContentToMainFrame);
				}
			}else{
				//loginView.loginMessage.html(data.returnMessage);
				//loginView.showLoginMessage();
				showGenericErrorMessage(data.returnMessage);
			}
		},
		afterSignIn : function(){
			//automatska prijav u sustav nakon registracije
			var regFormObject = getFormObject("registrationFrame");
			var inputData = new Object();
			inputData.ussername = regFormObject.usernameReg;
			inputData.password = regFormObject.passwordReg;
			getAjaxCall("loginHandler/login",inputData,null,loginController.afterLogin);
		},
		signinCallback : function(){
			gapi.client.load('plus','v1', function(){
	      		 var request = gapi.client.plus.people.get({
	      		   'userId': 'me'
	      		 });
	      		 request.execute(function(resp) {		      			 
	      		   getAjaxCallAnonimus("loginHandler/googleSignIn", resp, null, loginController.afterLogin);
	      		 });
	      	});
		},
		googleLogin_onClick : function(){
			showAjaxSpiner();
			sessionParams = {
					'client_id': '175081916192-gvern42vs47vth21q1j507a1l7i4gtj0.apps.googleusercontent.com',
					'session_state': null
			};
			gapi.auth.checkSessionState(sessionParams, function(stateMatched) { 
				if (stateMatched == true) {
					gapi.auth.authorize({client_id: '175081916192-gvern42vs47vth21q1j507a1l7i4gtj0.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/plus.login', immediate: false}, loginController.signinCallback);				
				} else {					
					gapi.auth.authorize({client_id: '175081916192-gvern42vs47vth21q1j507a1l7i4gtj0.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/plus.login', immediate: true}, loginController.signinCallback);			      
				}
			});			
		},
		openMailDialog_onClick : function(){
			loginView.mailModal.modal();
		},
		sendMail_onClick : function(){		
			var addressEmail = loginView.emailInput.val();
			getAjaxCallAnonimus("loginHandler/sendMail", addressEmail, null, loginView.afterMail);
		}
};

var loginView = {
	btnPrijava : null,
	btnRegistracija : null,
	loginMessage : null,
	mainFrame : null,
	zapamtiMe : null,
	passwordInput : null,
	btnGoogleLogin : null,
	mailModal : null,
	mailLink : null,
	btnSendMail : null,
	emailInput : null,
	linkPrijavaRegistracija : null,
	init : function(){		
		indexView.hideFilterMyGames();
		indexView.hideFilterMyProfile();
		this.mainFrame = $("#mainFrame");
		this.mailModal = $("#mailModal");
		this.mailLink = $("#mailLink");
		this.emailInput = $("#email");
		this.btnSendMail = $("#btnSendMail");
		this.btnSendMail.click(loginController.sendMail_onClick);
		//this.loginMessage = $("#loginMessage");
		//this.hideLoginMessage();
		this.btnPrijava = $("#btnPrijava");
		this.btnPrijava.click(loginController.btnPrijava_onclick);
		this.btnRegistracija = $("#btnRegistracija");
		this.btnRegistracija.click(loginController.btnRegistracija_onclick);
		this.zapamtiMe = $("#zapamtiMe");
		this.passwordInput = $("#password");
		this.passwordRegInput = $("#passwordReg");
		this.btnGoogleLogin = $("#btnGoogleLogin");
		$(".kontaktLink").click(indexController.kontakt_onClick);
		this.linkPrijavaRegistracija = $(".linkPrijavaRegistracija");
		loginView.linkPrijavaRegistracija.click(indexController.prijava_onClick);
		loginView.passwordInput.keyup(function(event){
		    if(event.keyCode == 13){
		    	loginView.btnPrijava.click();
		    }
		});
		loginView.passwordRegInput.keyup(function(event){
		    if(event.keyCode == 13){
		    	loginView.btnRegistracija.click();
		    }
		});
		this.btnGoogleLogin.click(loginController.googleLogin_onClick);
		this.mailLink.click(loginController.openMailDialog_onClick);
	},
	
	setContentToMainFrame : function(screen){
		this.mainFrame.html(screen);
	},
	
	showLoginMessage : function(){
		this.loginMessage.show();
	},
	
	hideLoginMessage : function(){
		this.loginMessage.hide();
	},
	afterMail : function(data){
		if(data.errorMessage != undefined){
			showGenericErrorMessage(data.errorMessage);
		}
		if(data.returnMessage != undefined){
			showGenericOkMessage(data.returnMessage);			
		}
		
	}
};

var loginModel = {		
		init : function(){
			
		}
};