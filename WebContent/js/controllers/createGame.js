/**
 * 
 */
var createGameController = {
	init : function(){
		$(document).ready(function() {
			createGameView.init();
		});
	},
	afterInsertGame : function(param){
		clearForm("createGameFrame");
		showGenericOkMessage(param.returnMessage);
		getScreen("myGames.html", null, indexView.setContentToMainFrame);
	},
	btnKreirajUtakmicu_onClick : function(){
		if(!isValidationError("createGameFrame")){
			getAjaxCall("gameHandler/insertGame", getFormObject("createGameFrame"), null, createGameController.afterInsertGame);
		}
	},
	inputBlagajnik_onKeyUp : function(){		
	},
	setTypeAheadSource : function(returnedData, process){
		users = [];
	    map = {};
	    $.each(returnedData, function (i, user) {
	        map[user.ime+" "+user.prezime] = user;
	        users.push(user.ime+" "+user.prezime);
	    });
		process(users);
	},
	onClick_obracunTroskova : function(){
		if(createGameView.placanjeDiv.css('display') != 'none'){
			createGameView.placanjeDiv.hide();
		}else{
			createGameView.placanjeDiv.show();
		}
	},
	onClick_prikaziLozinku : function(){		
		if(createGameView.lozinka.css('display') != 'none'){
			createGameView.lozinka.hide();
		}else{
			createGameView.lozinka.show();
		}
	}
};

var createGameView = {
		danGame : null,
		mjesecGame : null,
		yearGame : null,
		satGame : null,
		minGame : null,
		blagajnik : null,
		btnKreirajUtakmicu : null,
		obracunTroskova : null,
		placanjeDiv : null,
		prikaziLozinku : null,
		lozinka : null,
		init : function(){
			indexView.hideFilterMyGames();
			indexView.hideFilterMyProfile();
			this.danGame = $("#danGame");
			this.mjesecGame = $("#mjesecGame");
			this.yearGame = $("#godinaGame");
			this.satGame = $("#satGame");
			this.minGame = $("#minGame");
			this.blagajnik = $("#blagajnik");
			this.blagajnikId = $("#blagajnikId");
			this.cijenaTerena = $("#cijenaTerenaDiv");
			this.btnKreirajUtakmicu = $("#btnKreirajUtakmicu");
			this.btnKreirajUtakmicu.click(createGameController.btnKreirajUtakmicu_onClick);
			this.obracunTroskova = $("#obracunTroskova");
			this.placanjeDiv = $("#placanjeDiv");
			this.prikaziLozinku =$("#prikaziLozinku");
			this.lozinka = $("#lozinka");
			this.setDate();
			this.setTime();
			this.blagajnik.typeahead({
				minLength : 1,
			    source: function (query, process) {
			    	timer = null;
			    	clearTimeout(timer);					
			    	timer = setTimeout(getAjaxCall("loginHandler/searchUsers", $("#blagajnik").val(), null, createGameController.setTypeAheadSource, process), odziv);
			    },
				updater: function (item) {
					createGameModel.blagajnikTypeAheadSelectedValue = map[item];
					createGameView.showCijenaField();
					createGameView.blagajnikId.val(map[item].idUser);
				    return item;
				}
			});
			this.obracunTroskova.click(createGameController.onClick_obracunTroskova);
			this.prikaziLozinku.click(createGameController.onClick_prikaziLozinku);
		},
		setDate : function(){
			for(var i=1; i<=31; i++){
				this.danGame.append($("<option></option>").attr("value",i).text(i));
			}
			for(var i=1; i<=12; i++){
				this.mjesecGame.append($("<option></option>").attr("value",i).text(i));
			}
			var today = new Date();
			var yyyy = today.getFullYear();
			
			for(var i=yyyy; i<yyyy+15; i++){
				this.yearGame.append($("<option></option>").attr("value",i).text(i));
			}
			selectValueByKey("danGame", today.getDate());
			selectValueByKey("mjesecGame", today.getMonth()+1);
			selectValueByKey("yearGame", today.getFullYear());
		},
		setTime : function(){
			for(var i=1; i<=24; i++){
				if(i<10)
					this.satGame.append($("<option></option>").attr("value",i).text('0'+i));
				else
					this.satGame.append($("<option></option>").attr("value",i).text(i));
			}
			for(var i=0; i<60; i++){
				if(i<10)
					this.minGame.append($("<option></option>").attr("value",i).text('0'+i));
				else
					this.minGame.append($("<option></option>").attr("value",i).text(i));
			}
		},
		hideCijenaField : function(){
			this.cijenaTerena.hide();
		},
		showCijenaField : function(){
			this.cijenaTerena.show();
		}
};

var createGameModel = {
		blagajnikTypeAheadSelectedValue : null
};