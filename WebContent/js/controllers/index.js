/**
 * Main js class for index screen
 */
var indexController = {		
		//
		//Entry point for screen object
		init : function(){			
			$(document).ready(function() {
				indexView.init();
				indexController.checkIfIsAndroid();
				//Provjeri ako je korisnik ostao zapamcen i ako je redirect to main page
				if(indexController.checkIfUserHaveSession()){
					//Redirectaj na glavni ekran
					getScreen("myGames.html",null,indexView.setContentToMainFrame);					
				}else{
					getScreen("login.html", null, indexView.setContentToMainFrame);
				}
				
				if(window.location.search.substring(1) != ""){
					var paramName = window.location.search.substring(1).split("=")[0];					
					if(paramName == "gameId"){
						urlGameId = window.location.search.substring(1).split("=")[1];
					}
				}
			});
		},
		checkIfIsAndroid : function(){
			if(localStorage.getItem("androidAppKnow") != "yes"){
				var ua = navigator.userAgent.toLowerCase();
				var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
				if(isAndroid) {
					indexView.androidModal.modal();
				}
			}
		},
		checkIfUserHaveSession : function(){
			if(localStorage.getItem("securityToken") != undefined){
				sessionStorage.setItem("ime",localStorage.getItem("ime"));
				sessionStorage.setItem("prezime",localStorage.getItem("prezime"));
				sessionStorage.setItem("securityToken",localStorage.getItem("securityToken"));
				sessionStorage.setItem("userId",localStorage.getItem("userId"));
				sessionStorage.setItem("grupa",localStorage.getItem("grupa"));				
				return true;
			}else{
				return false;
			}
		},
		filterMojeUtakmice_onClick : function(){
			myGamesView.clearGamesList();
			myGamesController.getGamesInputDto.activeFilter = "mojeUtakmice";
			myGamesController.getGamesInputDto.startPosition = 0;
			myGamesController.getGamesInputDto.maxPosition = 6;
			myGamesController.getGames(myGamesController.getGamesInputDto);
		},
		danasnjeUtakmiceFilter_onClick : function(){
			myGamesView.clearGamesList();
			myGamesController.getGamesInputDto.activeFilter = "danasnjeUtakmice";
			myGamesController.getGamesInputDto.startPosition = 0;
			myGamesController.getGamesInputDto.maxPosition = 6;
			myGamesController.getGames(myGamesController.getGamesInputDto);
		},
		sveUtakmiceFilter_onClick : function(){
			myGamesView.clearGamesList();
			myGamesController.getGamesInputDto.activeFilter = "sveUtakmice";
			myGamesController.getGamesInputDto.startPosition = 0;
			myGamesController.getGamesInputDto.maxPosition = 6;
			myGamesController.getGames(myGamesController.getGamesInputDto);
		},
		mojiPodaci_onChange : function(){
			myProfileView.hideAllTabs();
			myProfileView.mojiPodaciTab.show();
		},
		komeSamJaDuzan_onChange : function(){
			myProfileView.hideAllTabs();
			myProfileView.komeSamJaDuzanTab.show();
			//Dohvat kome sam ja dužan
			getAjaxCall("gameHandler/getKomeSamDuzan", null, null, myProfileView.fillKomeSamDuzan);
		},
		tkoJeMeniDuzan_onChange : function(){
			myProfileView.hideAllTabs();
			myProfileView.tkoMiJeDuzanTab.show();
			//Dohvat dužnika
			getAjaxCall("gameHandler/getDuznici", null, null, myProfileView.fillDuznici);
		},
		kontakt_onClick : function(){
			indexView.liKontakt.addClass("active");
			indexView.liPrijavaRegistracija.removeClass("active");
			getScreen("kontakt.html", null, indexView.setContentToMainFrame);
		},
		prijava_onClick : function(){
			indexView.liKontakt.removeClass("active");
			indexView.liPrijavaRegistracija.addClass("active");
			getScreen("login.html", null, indexView.setContentToMainFrame);
		},
		btnDontAndroid_onClick : function(){
			localStorage.setItem("androidAppKnow","yes");
		}
};

var indexView = {
	menuItem1 : "<li class='liPrijavaRegistracija' class='active'><a id='linkPrijavaRegistracija' class='linkPrijavaRegistracija prijavaLink standardApplicationLink' data-toggle='collapse' data-target='#menuList'>Prijava / Registracija</a></li>",
	menuItem2 : "<li class='liKontakt'><a id='linkKontakt' class='kontaktLink standardApplicationLink' data-toggle='collapse' data-target='#menuList'>Kontakt</a></li>",
	mainMenu :	null,
	mainMenuMob : null,
	logedUser : null,
	danasnjeUtakmice : null,
	mojeUtakmice : null,
	sveUtakmice : null,
	mojeUtakmiceFilter : null,
	danasnjeUtakmiceFilter : null,
	sveUtakmiceFilter : null,
	mojiPodaciTab : null,
	tkoMiJeDuzanTab : null,
	komeSamJaDuzanTab : null,
	liKontakt : null,
	liPrijavaRegistracija : null,
	linkPrijavaRegistracija : null,
	androidModal : null,
	btnDontAndroid : null,
	init : function(){
		this.androidModal = $("#androidModal");
		this.mojeUtakmiceFilter = $("#mojeUtakmiceFilter");
		this.danasnjeUtakmiceFilter = $("#danasnjeUtakmiceFilter");
		this.sveUtakmiceFilter = $("#sveUtakmiceFilter");
		this.mojiPodaciLi = $("#mojiPodaciFilter");
		this.tkoMiJeDuzanLi = $("#tkoMiJeDuzanFilter");
		this.KomeSamJaDuzanLi = $("#komeSamJaDuzanFilter");
		this.mainMenu = $("#mainMenu ul");
		this.mainMenuMob = $("#mainMenuMob ul");
		this.logedUser = $("#logedUser");
		this.danasnjeUtakmice = $("#danasnjeUtakmice");
		this.mojeUtakmice = $("#mojeUtakmice");
		this.sveUtakmice = $("#sveUtakmice");
		this.btnDontAndroid = $("#btnDontAndroid");
		indexView.clearMenu();
		indexView.addToMenu(this.menuItem1);
		indexView.addToMenu(this.menuItem2);
		indexView.hideFilterMyGames();
		indexView.hideFilterMyProfile();
		this.liKontakt = $(".liKontakt");
		this.liPrijavaRegistracija = $(".liPrijavaRegistracija");		
		indexView.liKontakt.removeClass("active");
		indexView.liPrijavaRegistracija.addClass("active");
		//Handleri
		indexView.mojeUtakmiceFilter.change(indexController.filterMojeUtakmice_onClick);
		indexView.danasnjeUtakmiceFilter.change(indexController.danasnjeUtakmiceFilter_onClick);
		indexView.sveUtakmiceFilter.change(indexController.sveUtakmiceFilter_onClick);
		indexView.mojiPodaciLi.change(indexController.mojiPodaci_onChange);
		indexView.KomeSamJaDuzanLi.change(indexController.komeSamJaDuzan_onChange);
		indexView.tkoMiJeDuzanLi.change(indexController.tkoJeMeniDuzan_onChange);	
		this.btnDontAndroid.click(indexController.btnDontAndroid_onClick);
	},
	setContentToMainFrame : function(screen){		
		$("#mainFrame").html(screen);
	},
	showFilterMyGames : function(){
		indexView.hideFilterMyProfile();
		$("#filterMyGames").show();
	},
	hideFilterMyGames : function(){
		$("#filterMyGames").hide();
	},
	showFilterMyProfile : function(){
		indexView.hideFilterMyGames();
		$("#filterMyProfile").show();
	},
	hideFilterMyProfile : function(){
		$("#filterMyProfile").hide();
	},
	clearMenu : function(){
		this.mainMenu.html("");
		this.mainMenuMob.html("");
	},
	addToMenu : function(addElem){
		if(this.mainMenuMob.html() != ""){
			this.mainMenuMob.append(addElem);
		}else{
			this.mainMenuMob.html(addElem);
		}
		if(this.mainMenu.html() != ""){
			this.mainMenu.append(addElem);
		}else{
			this.mainMenu.html(addElem);
		}
	},
	selectDanasnjeUtakmice : function(){
		indexView.danasnjeUtakmice.addClass("active");
	},
	deselectSviFilteri : function(){
		indexView.danasnjeUtakmice.removeClass("active");
		indexView.sveUtakmice.removeClass("active");
		indexView.mojeUtakmice.removeClass("active");
	}
	
};

var indexModel = {		
		
};