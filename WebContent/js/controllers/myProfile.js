var myProfileController = {
		selectedUserId : null,
		inputId : null,
		init : function(){
			$(document).ready(function() {
				myProfileView.init();
				myProfileController.loadUserData();
			});			
		},		
		loadUserData : function(){			
			getAjaxCall("loginHandler/getUserById", sessionStorage.getItem("userId"), null, myProfileView.fillFormWithUserData);
			
		},
		btnAzurirajPodatke_onClick : function(){			
			var formData = getFormObject("mojiPodaciTab");
			if(myProfileView.sendMail.prop('checked')){
				formData.sendMail = "yes";
			}else{
				formData.sendMail = "no";
			}
			getAjaxCall("loginHandler/editUser", formData, null, myProfileView.afterEditUserData);
		},
		btnUplataKorisniku_onClick : function(idElem, idKorisnika, imePrezime){			
			var iznosUplate = $("#inputUplata_"+idElem).val();			
			swal({
				title: "Jeste li sigurni da želite izvršiti uplatu?",   
				text: "Za korisnika "+imePrezime+" uplatit će te "+iznosUplate+"Kn.",   
				type: "warning",   
				showCancelButton: true,   
				cancelButtonText: "Ne želim",
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Želim!",  				
				closeOnConfirm: true }, 
				function(){   
					var inputDto = new Object();
					inputDto.idKorisnika = idKorisnika;
					inputDto.iznosUplate = iznosUplate;					
					getAjaxCall("gameHandler/setUplata", inputDto, null, myProfileView.afterInsertUplata); 
				});
		},
		btnUplataModal_onClick : function(idElem, idKorisnika, imePrezime){
			myProfileView.uplataModal.modal();
			myProfileView.korisnikUplataModal.html(imePrezime);
			myProfileView.uplataFieldModal.html(myProfileView.addBtnUplate(idElem+100));
			myProfileController.inputId = idElem+100;
			myProfileController.selectedUserId = idKorisnika;
		},
		btnOpenFolder_onClick : function(){
			myProfileView.inputAvatar.click();
		},
		inputAvatar_onChange : function(){
			var fileInput = document.getElementById('inputAvatar');
			fileLocal = fileInput.files[0];
			myProfileView.inputAvatarName.val(fileLocal.name);
			getAjaxUpload("loginHandler/uploadProfilePicture",fileLocal,null,myProfileView.postUpload);
		},
		btnDeleteAvatar_onClick : function(){
			getAjaxCall("loginHandler/deleteAvatar", null, null, myProfileView.postAvatarDelete);
		},
		btnUplataDialog_onClick : function(){
			var iznosUplate = $("#inputUplata_"+myProfileController.inputId).val();	
			var inputDto = new Object();
			inputDto.idKorisnika = myProfileController.selectedUserId;
			inputDto.iznosUplate = iznosUplate;
			getAjaxCall("gameHandler/setUplata", inputDto, null, myProfileView.afterInsertUplata);			
		},
		linkMerge_onClick : function(){
			myProfileView.mergeModal.modal();
		},
		btnMerge_onClick : function(){
			getAjaxCall("loginHandler/mergeUser", getFormObject("mergeModal"), null, myProfileView.afterMergeUser);
		}
};

var myProfileView = {		
		mojiPodaciLi : null, 
		tkoMiJeDuzanLi : null,
		KomeSamJaDuzanLi : null,
		btnAzurirajPodatke : null,
		btnOpenFolder : null,
		//Tabovi		
		komeSamDuzanBody : null,
		duzniciBody : null,
		inputAvatar : null,
		inputAvatarName : null,
		profilePicture : null,
		imgPre : null,
		btnDeleteAvatar : null,
		uplataModal : null,
		korisnikUplataModal : null,
		uplataFieldModal : null,
		btnUplataDialog : null,
		mergeDiv : null,
		mergeModal : null,
		mergeLink : null,
		btnMerge : null,
		sendMail : null,
		saldoKomeSamDuzan : null,
		saldoDuznika : null,
		duznici : null,
		vjerovnici : null,
		mojiPodaci : null,
		init : function(){			
			indexView.showFilterMyProfile();			
			this.btnAzurirajPodatke = $("#btnEditOsobniPodaci");
			this.duzniciBody = $("#duzniciBody");
			this.komeSamDuzanBody = $("#komeSamDuzanBody");
			this.btnOpenFolder = $("#btnOpenFolder");
			this.inputAvatar = $("#inputAvatar");
			this.inputAvatarName = $("#avatarName");
			this.imgPre = $("#imgPre");
			this.profilePicture = $("#profilePicture");
			this.btnDeleteAvatar = $("#btnDeleteAvatar");
			this.uplataModal = $("#uplataModal");
			this.mergeModal = $("#mergeModal");
			this.korisnikUplataModal = $("#korisnikUplataModal");
			this.uplataFieldModal = $("#uplataFieldModal");
			this.btnUplataDialog = $("#btnUplataDialog");
			this.mergeDiv = $("#mergeDiv");
			this.mergeLink = $("#mergeLink");
			this.btnMerge = $("#btnMerge");
			this.sendMail = $("#sendMail");
			this.saldoDuznika = $("#saldoDuznika");
			this.saldoKomeSamDuzan = $("#saldoKomeSamDuzan");
			this.mergeDiv.hide();
			this.duznici = $("#duznici");
			this.vjerovnici = $("#vjerovnici");
			this.mojiPodaci = $("#mojiPodaci");
			//Tabovi
			this.mojiPodaciTab = $("#mojiPodaciTab");
			this.tkoMiJeDuzanTab = $("#tkoMiJeDuzanTab");
			this.komeSamJaDuzanTab = $("#komeSamJaDuzanTab");
			this.mojiPodaciTab.show();
			this.komeSamJaDuzanTab.hide();
			this.tkoMiJeDuzanTab.hide();
			//Handleri			
			this.btnAzurirajPodatke.click(myProfileController.btnAzurirajPodatke_onClick);
			this.btnOpenFolder.click(myProfileController.btnOpenFolder_onClick);
			this.inputAvatar.change(myProfileController.inputAvatar_onChange);
			this.btnDeleteAvatar.click(myProfileController.btnDeleteAvatar_onClick);
			this.btnUplataDialog.click(myProfileController.btnUplataDialog_onClick);	
			this.mergeLink.click(myProfileController.linkMerge_onClick);
			this.btnMerge.click(myProfileController.btnMerge_onClick);
			myProfileView.deselectSviFilteri();
		},
		addBtnUplate : function(id){
			return "<div class='input-group uplateInput'>" +
					"<span class='input-group-btn'>" +
					"<button class='btn btn-default btnUplata' onclick='myProfileView.decrementUplataInput("+id+")'>-</button>" +					
					"</span>" +
					"<input id='inputUplata_"+id+"' type='text' class='form-control inputUplata' value='25'>" +
					"<span class='input-group-btn'>" +
					"<button class='btn btn-default btnUplata' onclick='myProfileView.incrementUplataInput("+id+")'>+</button>" +
					"</div>";
		},
		deselectSviFilteri : function(){
			myProfileView.vjerovnici.removeClass("active");
			myProfileView.duznici.removeClass("active");
			myProfileView.mojiPodaci.addClass("active");
		},
		decrementUplataInput : function(id){
			var val = $("#inputUplata_"+id).val();
			val = val - 1;
			$("#inputUplata_"+id).val(val);
		},
		incrementUplataInput : function(id){
			var val = $("#inputUplata_"+id).val();
			val = parseInt(val) + parseInt(1);
			$("#inputUplata_"+id).val(val);
		},
		appendDuzniciRow : function(redniBroj, ime, prezime, iznos, idKorisnika){
			var imePrezime = ime + " " + prezime;					
			if(iznos > 0 ){
				myProfileView.duzniciBody.append("<tr>" +
						"<td class='hidden-xs uplateTd'>"+redniBroj+"</td>" +
						"<td class='uplateTd'>"+ime+" "+prezime+"</td>" +
						"<td class='uplateTd'><span class='label label-success'>"+iznos+"Kn</span></td>" +
						"<td class='uplateTd hidden-xs'>"+myProfileView.addBtnUplate(redniBroj)+"</td>" +
						"<td class='uplateTd hidden-xs'><button id='btnUplata_"+redniBroj+"' class='btn btn-success'>Uplati</button></td>" +
						"<td class='uplateTd hidden-lg hidden-md hidden-sm'><button id='btnUplataModal_"+redniBroj+"' class='btn btn-success'>Uplata</button></td>" +
				"</tr>");
			}else if(iznos < 0){
				myProfileView.duzniciBody.append("<tr>" +
						"<td class='hidden-xs uplateTd'>"+redniBroj+"</td>" +
						"<td class='uplateTd'>"+ime+" "+prezime+"</td>" +
						"<td class='uplateTd'><span class='label label-danger'>"+iznos+"Kn</span></td>" +
						"<td class='uplateTd hidden-xs'>"+myProfileView.addBtnUplate(redniBroj)+"</td>" +
						"<td class='uplateTd hidden-xs'><button id='btnUplata_"+redniBroj+"' class='btn btn-success'>Uplati</button></td>" +
						"<td class='uplateTd hidden-lg hidden-md hidden-sm'><button id='btnUplataModal_"+redniBroj+"' class='btn btn-success'>Uplata</button></td>" +
				"</tr>");
			}
			$("#btnUplata_"+redniBroj).click(function(){				
				myProfileController.btnUplataKorisniku_onClick(redniBroj, idKorisnika, imePrezime);
			});	
			$("#btnUplataModal_"+redniBroj).click(function(){				
				myProfileController.btnUplataModal_onClick(redniBroj, idKorisnika, imePrezime);
			});
		},
		appendKomeSamDuzanRow : function(redniBroj, ime, prezime, iznos){
			if(iznos > 0 ){
				myProfileView.komeSamDuzanBody.append("<tr>" +
						"<td class='hidden-xs uplateTd'>"+redniBroj+"</td>" +
						"<td class='uplateTd'>"+ime+" "+prezime+"</td>" +
						"<td class='uplateTd'><span class='label label-success'>"+iznos+"Kn</span></td>" +
								"</tr>");
			}else if(iznos < 0){
				myProfileView.komeSamDuzanBody.append("<tr>" +
						"<td class='hidden-xs uplateTd'>"+redniBroj+"</td>" +
						"<td class='uplateTd'>"+ime+" "+prezime+"</td>" +
						"<td class='uplateTd'><span class='label label-danger'>"+iznos+"Kn</span></td>" +
								"</tr>");
			}
		},
		clearDuzniciTable : function(){
			myProfileView.duzniciBody.html("");
		},
		clearKomeSamDuzanTable : function(){
			myProfileView.komeSamDuzanBody.html("");
		},
		hideAllTabs : function(){
			myProfileView.mojiPodaciTab.hide();
			myProfileView.komeSamJaDuzanTab.hide();
			myProfileView.tkoMiJeDuzanTab.hide();
		},
		fillFormWithUserData : function(data){
			fillForm(data);
			if(data.sendMail == "yes"){
				myProfileView.sendMail.prop('checked', true);
			}else{
				myProfileView.sendMail.prop('checked', false);
			}
			if(data.existAvatar == "true"){
				myProfileView.showImgPresentation();
				myProfileView.addProfileImg(data.idUser);
			}else{
				myProfileView.hideImgPresentation();
			}			
			if(data.isMerged == "yes"){
				myProfileView.mergeDiv.hide();
			}else if(data.isMerged == "no"){
				myProfileView.mergeDiv.show();
			}
		},
		afterEditUserData : function(data){
			showGenericOkMessage("Korisnički podaci uspješno ažurirani.");
		},
		fillDuznici : function(data){
			myProfileView.clearDuzniciTable();
			var listaDuznika = data.listaDuznika;
			var redniBroj = 1;
			var saldoDuznika = 0;
			for(var i=0; i<listaDuznika.length; i++){
				if(listaDuznika[i].iznos != 0){
					myProfileView.appendDuzniciRow(redniBroj, listaDuznika[i].ime, listaDuznika[i].prezime, listaDuznika[i].iznos, listaDuznika[i].idKorisnika);
					saldoDuznika = saldoDuznika + parseFloat(listaDuznika[i].iznos);
					redniBroj++;
				}
			}
			if(saldoDuznika < 0){
				saldoDuznika = Math.abs(saldoDuznika);
				var currencySaldo = toCurrency(saldoDuznika);
				myProfileView.saldoDuznika.html("-"+currencySaldo);
			}else{
				myProfileView.saldoDuznika.html(toCurrency(saldoDuznika));
			}			
		},
		fillKomeSamDuzan : function(data){
			myProfileView.clearKomeSamDuzanTable();
			var listaKomeSamDuzan = data.listaDuznika;
			var redniBroj = 1;
			var saldoDuznika = 0;
			for(var i=0; i<listaKomeSamDuzan.length; i++){
				if(listaKomeSamDuzan[i].iznos != 0){
					myProfileView.appendKomeSamDuzanRow(redniBroj, listaKomeSamDuzan[i].ime, listaKomeSamDuzan[i].prezime, listaKomeSamDuzan[i].iznos);
					saldoDuznika = saldoDuznika + parseFloat(listaKomeSamDuzan[i].iznos);
					redniBroj++;
				}
			} 
			if(saldoDuznika < 0){
				saldoDuznika = Math.abs(saldoDuznika);
				var currencySaldo = toCurrency(saldoDuznika);
				myProfileView.saldoKomeSamDuzan.html("-"+currencySaldo);
			}else{
				myProfileView.saldoKomeSamDuzan.html(toCurrency(saldoDuznika));
			}
		},
		afterInsertUplata : function(data){
			showGenericOkMessage(data.returnMessage);
			myProfileView.fillDuznici(data);
		},
		postUpload : function(returnedData){
			myProfileView.showImgPresentation();
			d = new Date();
			myProfileView.profilePicture.attr("src","img/" + sessionStorage.getItem("userId") + "_profileImg.jpg?" + d.getTime());
		},
		hideImgPresentation : function(){
			myProfileView.imgPre.hide();
		},
		showImgPresentation : function(){
			myProfileView.imgPre.show();
		},
		addProfileImg : function(userId){
			d = new Date();
			myProfileView.profilePicture.attr("src","img/" + userId + "_profileImg.jpg?" + d.getTime());
		},
		postAvatarDelete : function(){
			myProfileView.hideImgPresentation();
		},
		afterMergeUser : function(data){
			sessionStorage.setItem("userId", data.mergeUserId);
			sessionStorage.setItem("grupa", data.mergeGroupId);			
			myProfileController.loadUserData();
			showGenericOkMessage(data.returnMessage);
		}
};